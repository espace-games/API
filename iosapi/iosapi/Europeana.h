//
//  Europeana.h
//  iosapi
//
//  Created by Terry Goodwin on 02/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifndef iosapi_Europeana_h
#define iosapi_Europeana_h

@interface EuropeanaObject : NSObject {
    int _index;
    NSString* _objectId;
    NSString* _type;
    NSString* _title;
    NSString* _description;
    NSString* _attribution;
    NSString* _provider;
    NSString* _credit;
    UIImage* _textureData;
}

-(int)index;
-(void)setIndex:(int)newValue;

-(NSString*)objectId;
-(void)setObjectId:(NSString*)newValue;

-(NSString*)type;
-(void)setType:(NSString*)newValue;

-(NSString*)title;
-(void)setTitle:(NSString*)newValue;

-(NSString*)description;
-(void)setDescription:(NSString*)newValue;

-(NSString*)attribution;
-(void)setAttribution:(NSString*)newValue;

-(NSString*)provider;
-(void)setProvider:(NSString*)newValue;

-(NSString*)credit;
-(void)setCredit:(NSString*)newValue;

-(UIImage*)textureData;
-(void)setTextureData:(UIImage*)newValue;

-(void)generateAttribution;

-(id)initWithId:(NSString*)europeanaId andType:(NSString*)newType;
-(id)initWithPList:(NSString*)europeanaId;

-(bool)writeToPList;
-(bool)readFromPListWithId:(NSString*)europeanaId;

@end

typedef void(^EuropeanaObjectCallback)(EuropeanaObject*);
typedef void(^ArrayCallback)(NSArray*);
typedef void(^IntCallback)(int);

@interface EuropeanaRequest : NSObject {
    bool _verbose;
    bool _ended;
    EuropeanaRequest* _owner;
    NSDictionary* _options;
    NSMutableArray* _subRequests;
    NSArray* _archiveObjects;
    EuropeanaObject* _archiveObject;
    
    EuropeanaObjectCallback _singleCallback;
    ArrayCallback _totalCallback;
    EuropeanaObjectCallback _recordCallback;
    EuropeanaObjectCallback _mediaCallback;
    IntCallback _errorCallback;
    
    NSMutableURLRequest* _www;
    int _error;
    NSData* _responseData;
    NSDictionary* _responseHeaders;
}

-(bool)verbose;
-(void)setVerbose:(bool)newValue;

-(bool)ended;
-(void)setEnded:(bool)newValue;

-(EuropeanaRequest*)owner;
-(void)setOwner:(EuropeanaRequest*)newValue;

-(NSDictionary*)options;
-(void)setOptions:(NSDictionary*)newValue;

-(NSMutableArray*)subRequests;
-(void)setSubRequests:(NSMutableArray*)newValue;

-(NSArray*)archiveObjects;
-(void)setArchiveObjects:(NSArray*)newValue;

-(EuropeanaObject*)archiveObject;
-(void)setArchiveObject:(EuropeanaObject*)newValue;

-(EuropeanaObjectCallback)singleCallback;
-(void)setSingleCallback:(EuropeanaObjectCallback)newValue;

-(ArrayCallback)totalCallback;
-(void)setTotalCallback:(ArrayCallback)newValue;

-(EuropeanaObjectCallback)recordCallback;
-(void)setRecordCallback:(EuropeanaObjectCallback)newValue;

-(EuropeanaObjectCallback)mediaCallback;
-(void)setMediaCallback:(EuropeanaObjectCallback)newValue;

-(IntCallback)errorCallback;
-(void)setErrorCallback:(IntCallback)newValue;

-(NSMutableURLRequest*)www;
-(void)setWww:(NSMutableURLRequest*)newValue;

-(int)error;
-(void)setError:(int)newValue;

-(NSData*)responseData;
-(void)setResponseData:(NSData*)newValue;

-(NSDictionary*)responseHeaders;
-(void)setResponseHeaders:(NSDictionary*)newValue;

-(id)init;
-(void)dealloc;

-(void)dispose;
-(void)clear;
-(void)addSubRequest:(EuropeanaRequest*)subRequest;
-(void)cancel;
-(void)end;
-(bool)checkEnded;

-(void)callSingleCallback:(EuropeanaObject*)single;
-(void)callTotalCallback:(NSArray*)total;
-(void)callRecordCallback:(EuropeanaObject*)record;
-(void)callMediaCallback:(EuropeanaObject*)mediaRecord;
-(void)callErrorCallback:(int)error;

-(void)endSubRequests;
-(void)removeSubRequest:(EuropeanaRequest*)subRequest;

@end

@interface Europeana : NSObject

+(NSString*) VALUE_TRUE;
+(NSString*) VALUE_FALSE;

+(NSString*) OPTION_ID;
+(NSString*) OPTION_SEARCH;
+(NSString*) OPTION_COUNT;
+(NSString*) OPTION_OFFSET;
+(NSString*) OPTION_WIDTH;
+(NSString*) OPTION_HEIGHT;
+(NSString*) OPTION_MIN_WIDTH;
+(NSString*) OPTION_MIN_HEIGHT;
+(NSString*) OPTION_KEEP_ASPECT;
+(NSString*) OPTION_POWER_OF_2;
+(NSString*) OPTION_CROP_PERCENTAGE;
+(NSString*) OPTION_TEXTURE_SIZE;
+(NSString*) OPTION_VERBOSE_LOGGING;
+(NSString*) OPTION_NO_MEDIA;

+(int) ERROR_NONE;
+(int) ERROR_NO_RESULTS;
+(int) ERROR_NONE_FOUND;
+(int) ERROR_REQUEST;
+(int) ERROR_BAD_DATA;
+(int) ERROR_NO_TERMS;
+(int) ERROR_NO_TYPE;
+(int) ERROR_CANCELLED;
+(int) ERROR_BAD_ARCHIVE;
+(int) ERROR_RETRIEVING;
+(int) ERROR_MEDIA_REQUEST;

+(NSArray*) ERRORS;

+(NSString*) UNKNOWN_ERROR;

+(NSString*) API_URL;
+(NSString*) API_PORT;

+(NSString*) PARAM_QUERY;
+(NSString*) PARAM_SEPERATOR;
+(NSString*) PARAM_EQUALS;

+(NSString*) PARAM_TYPE;
+(NSString*) PARAM_SEARCH;
+(NSString*) PARAM_FIRST_INDEX;
+(NSString*) PARAM_COUNT;
+(NSString*) PARAM_MEDIA;
+(NSString*) PARAM_ONLY_AVAILABLE;
+(NSString*) PARAM_ID;
+(NSString*) PARAM_WIDTH;
+(NSString*) PARAM_HEIGHT;
+(NSString*) PARAM_MIN_WIDTH;
+(NSString*) PARAM_MIN_HEIGHT;
+(NSString*) PARAM_KEEP_ASPECT;
+(NSString*) PARAM_POWER_OF_2;
+(NSString*) PARAM_CROP_PERCENTAGE;

+(NSString*) TYPE_RECORDS;
+(NSString*) TYPE_RECORD;
+(NSString*) TYPE_IMAGE;
//    +(NSString*) TYPE_AUDIO; // Not ready for iOS
//    +(NSString*) TYPE_VIDEO; // Not ready for iOS

+(NSString*) RESULT_ITEMS;
+(NSString*) RESULT_TITLE;
+(NSString*) RESULT_DESCRIPTION;
+(NSString*) RESULT_ATTRIBUTION;
+(NSString*) RESULT_PROVIDER;
+(NSString*) RESULT_CREDIT;

+(NSString*) RESPONSE_WARNING;

+(int) DEFAULT_COUNT;
+(int) DEFAULT_OFFSET;
+(int) DEFAULT_TEXTURE_SIZE;
+(NSString*) DEFAULT_VERBOSE;

+(int) NUM_INVALID;


+(NSString*) PrintError:(int)error;
+(EuropeanaRequest*) GetImages:(NSDictionary*)options withSingle:(EuropeanaObjectCallback)single andTotal:(ArrayCallback)total andError:(IntCallback)error;
+(EuropeanaRequest*) GetImages:(NSString*)search count:(int)count offset:(int)offset width:(int)width height:(int)height keepAspect:(bool)keepAspect powerOf2:(bool)powerOf2 cropPercentage:(int)cropPercentage textureSize:(int)textureSize single:(EuropeanaObjectCallback)single total:(ArrayCallback)total error:(IntCallback)error;
+(EuropeanaRequest*) GetImage:(NSDictionary*)options withCallback:(EuropeanaObjectCallback)callback andError:(IntCallback)error;
+(EuropeanaRequest*) GetImage:(NSString*)recordId width:(int)width height:(int)height keepAspect:(bool)keepAspect powerOf2:(bool)powerOf2 cropPercentage:(int)cropPercentage textureSize:(int)textureSize callback:(EuropeanaObjectCallback)callback error:(IntCallback)error;
+(EuropeanaRequest*) GetRecords:(NSString*)type options:(NSDictionary*)options single:(EuropeanaObjectCallback)single total:(ArrayCallback)total error:(IntCallback)error;
+(void) DoGetRecords:(NSString*)type request:(EuropeanaRequest*)request;
+(void) IterateRecords:(NSArray*)items index:(int)index type:(NSString*)type objects:(NSMutableArray*)objects request:(EuropeanaRequest*)request;
+(EuropeanaRequest*) GetRecord:(NSString*)recordId type:(NSString*)type request:(EuropeanaRequest*)request callback:(EuropeanaObjectCallback)callback;
+(EuropeanaRequest*) GetRecordWithOptions:(NSDictionary*)options recordId:(NSString*)recordId type:(NSString*)type request:(EuropeanaRequest*)request callback:(EuropeanaObjectCallback)callback;
+(void) DoGetRecord:(NSString*)recordId type:(NSString*)type request:(EuropeanaRequest*)request;
+(EuropeanaRequest*) GetMediaForRecord:(NSString*)recordId type:(NSString*)type recordObject:(EuropeanaObject*)recordObject request:(EuropeanaRequest*)request callback:(EuropeanaObjectCallback)callback;
+(void) DoGetMediaForRecord:(NSString*)recordId type:(NSString*)type recordObject:(EuropeanaObject*)recordObject request:(EuropeanaRequest*)request;
+(bool) BadWWWResponse:(EuropeanaRequest*)request;

+(NSData*) RetrieveDataFromURL:(NSString*)url withRequest:(EuropeanaRequest*)request;

+(bool) DeviceIsRetina;

@end

#endif
