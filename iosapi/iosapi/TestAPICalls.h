//
//  TestAPICalls.h
//  iosapi
//
//  Created by Terry Goodwin on 03/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//

#ifndef iosapi_TestAPICalls_h
#define iosapi_TestAPICalls_h

#import "Europeana.h"

@interface TestAPICalls : UIViewController {
    EuropeanaRequest* _myRequest;
    
    IBOutletCollection(UIImageView) NSArray* paintings;
    IBOutletCollection(UILabel) NSArray* labels;
    
    IBOutlet UIImageView* singlePainting;
    IBOutlet UILabel* singleLabel;
}

-(EuropeanaRequest*)myRequest;
-(void)setMyRequest:(EuropeanaRequest*)newValue;

-(void)start;

@end

#endif
