//
//  Europeana.m
//  iosapi
//
//  Created by Terry Goodwin on 02/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Europeana.h"

@implementation EuropeanaObject

-(int)index {
    return _index;
}
-(void)setIndex:(int)newValue {
    _index = newValue;
}

-(NSString*)objectId {
    return _objectId;
}
-(void)setObjectId:(NSString*)newValue {
    _objectId = newValue;
}

-(NSString*)type {
    return _type;
}
-(void)setType:(NSString*)newValue {
    _type = newValue;
}

-(NSString*)title {
    return _title;
}
-(void)setTitle:(NSString*)newValue {
    _title = newValue;
}

-(NSString*)description {
    return _description;
}
-(void)setDescription:(NSString*)newValue {
    _description = newValue;
}

-(NSString*)attribution {
    return _attribution;
}
-(void)setAttribution:(NSString*)newValue {
    _attribution = newValue;
}

-(NSString*)provider {
    return _provider;
}
-(void)setProvider:(NSString*)newValue {
    _provider = newValue;
    [self generateAttribution];
}

-(NSString*)credit {
    return _credit;
}
-(void)setCredit:(NSString*)newValue {
    _credit = newValue;
    [self generateAttribution];
}

-(UIImage*)textureData {
    return _textureData;
}
-(void)setTextureData:(UIImage*)newValue {
    _textureData = newValue;
}

-(void)generateAttribution {
    bool gotOne = false;
    
    NSMutableString* newAttribution = [[NSMutableString alloc] initWithString:@""];
    NSString* creditString = [self credit];
    NSString* providerString = [self provider];
    
    if (creditString != nil && ![creditString isEqual: [NSNull null]] && creditString.length > 0) {
        gotOne = true;
        
        [newAttribution appendString: creditString];
        if (providerString != nil && ![providerString isEqual: [NSNull null]] && providerString.length > 0) {
            [newAttribution appendString: @", "];
        }
    }
    if (providerString != nil && ![providerString isEqual: [NSNull null]] && providerString.length > 0) {
        gotOne = true;

        [newAttribution appendString: providerString];
    }
    
    if (gotOne) {
        [self setAttribution:newAttribution];
    }
    
}

-(void)initializeData {
    [self setObjectId:nil];
    [self setType:nil];

    [self setIndex:-1];
    [self setTitle:nil];
    [self setDescription:nil];
    [self setProvider:nil];
    [self setCredit:nil];
    [self setTextureData:nil];
}

-(id)initWithId:(NSString*)europeanaId andType:(NSString*)newType {
    self = [super init];
    
    if (self) {
        [self initializeData];

        [self setObjectId:europeanaId];
        [self setType:newType];
    }
    
    return self;
}

-(id)initWithPList:(NSString*)europeanaId {
    self = [super init];
    if (self) {
        [self initializeData];
        
        [self readFromPListWithId:europeanaId];
    }
    
    return self;
}

-(bool)writeToPList {
    @try {
        NSString* filename = [[self objectId] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        
        NSError* error;
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", filename]];
        
        NSFileManager* fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath: path])
        {
            NSString* bundle = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
            if (bundle != nil) {
                [fileManager copyItemAtPath:bundle toPath: path error:&error];
            }
        }
        
        NSMutableDictionary* data = [[NSMutableDictionary alloc] initWithContentsOfFile: path];
        if (data == nil) {
            data = [[NSMutableDictionary alloc] init];
        }
        [data setObject:[NSNumber numberWithInt:[self index]] forKey:@"index"];
        
        NSString* dataString = [self objectId];
        if (dataString == nil || [dataString isEqual: [NSNull null]]) {
            dataString = @"";
        }
        [data setObject:dataString forKey:@"objectId"];

        dataString = [self type];
        if (dataString == nil || [dataString isEqual: [NSNull null]]) {
            dataString = @"";
        }
        [data setObject:dataString forKey:@"type"];

        dataString = [self title];
        if (dataString == nil || [dataString isEqual: [NSNull null]]) {
            dataString = @"";
        }
        [data setObject:dataString forKey:@"title"];

        dataString = [self description];
        if (dataString == nil || [dataString isEqual: [NSNull null]]) {
            dataString = @"";
        }
        [data setObject:dataString forKey:@"description"];

        dataString = [self provider];
        if (dataString == nil || [dataString isEqual: [NSNull null]]) {
            dataString = @"";
        }
        [data setObject:dataString forKey:@"provider"];

        dataString = [self credit];
        if (dataString == nil || [dataString isEqual: [NSNull null]]) {
            dataString = @"";
        }
        [data setObject:dataString forKey:@"credit"];
        
        [data writeToFile:path atomically:YES];
        return true;
    } @catch (NSException* exception) {
        return false;
    }
}

-(bool)readFromPListWithId:(NSString*)europeanaId {
    NSString* currentId = [self objectId];
    if (currentId != nil && [currentId caseInsensitiveCompare:europeanaId] != NSOrderedSame) {
        return false; // Not this object!
    }
    
    [self setObjectId:europeanaId];
    
    @try {
        NSString* filename = [[self objectId] stringByReplacingOccurrencesOfString:@"/" withString:@""];
        
        NSError* error;
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist", filename]];

        NSFileManager* fileManager = [NSFileManager defaultManager];
        if (![fileManager fileExistsAtPath: path])
        {
            NSString* bundle = [[NSBundle mainBundle] pathForResource:filename ofType:@"plist"];
            if (bundle != nil) {
                [fileManager copyItemAtPath:bundle toPath: path error:&error];
            }
        }

        NSMutableDictionary* savedObject = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
        [self setIndex:[[savedObject objectForKey:@"index"] intValue]];
        
        NSString* dataString = [savedObject objectForKey:@"type"];
        if (dataString != nil) {
            [self setType:dataString];
        }
        dataString = [savedObject objectForKey:@"title"];
        if (dataString != nil) {
            [self setTitle:dataString];
        }
        dataString = [savedObject objectForKey:@"description"];
        if (dataString != nil) {
            [self setDescription:dataString];
        }
        dataString = [savedObject objectForKey:@"provider"];
        if (dataString != nil) {
            [self setProvider:dataString];
        }
        dataString = [savedObject objectForKey:@"credit"];
        if (dataString != nil) {
            [self setCredit:dataString];
        }
        return true;
    } @catch (NSException* exception) {
        if (exception != nil) {
            NSLog(@"EuropeanaObject readFromPListWithId exception: %@", [exception reason]);
            return false;
        }
    }
    
    return true;
}

@end


@implementation EuropeanaRequest

-(bool)verbose {
    return _verbose;
}
-(void)setVerbose:(bool)newValue {
    _verbose = newValue;
    
    if (newValue) {
        NSLog(@"Europeana VERBOSE");
    }
}

-(bool)ended {
    return _ended;
}
-(void)setEnded:(bool)newValue {
    _ended = newValue;
}

-(EuropeanaRequest*)owner {
    return _owner;
}
-(void)setOwner:(EuropeanaRequest*)newValue {
    _owner = newValue;
}

-(NSDictionary*)options {
    return _options;
}
-(void)setOptions:(NSDictionary*)newValue {
    _options = newValue;
}

-(NSMutableArray*)subRequests {
    return _subRequests;
}
-(void)setSubRequests:(NSMutableArray*)newValue {
    _subRequests = newValue;
}

-(NSArray*)archiveObjects {
    return _archiveObjects;
}
-(void)setArchiveObjects:(NSArray*)newValue {
    _archiveObjects = newValue;
}

-(EuropeanaObject*)archiveObject {
    return _archiveObject;
}
-(void)setArchiveObject:(EuropeanaObject*)newValue {
    _archiveObject = newValue;
}

-(EuropeanaObjectCallback)singleCallback {
    return _singleCallback;
}
-(void)setSingleCallback:(EuropeanaObjectCallback)newValue {
    _singleCallback = newValue;
}

-(ArrayCallback)totalCallback {
    return _totalCallback;
}
-(void)setTotalCallback:(ArrayCallback)newValue {
    _totalCallback = newValue;
}

-(EuropeanaObjectCallback)recordCallback {
    return _recordCallback;
}
-(void)setRecordCallback:(EuropeanaObjectCallback)newValue {
    _recordCallback = newValue;
}

-(EuropeanaObjectCallback)mediaCallback {
    return _mediaCallback;
}
-(void)setMediaCallback:(EuropeanaObjectCallback)newValue {
    _mediaCallback = newValue;
}

-(IntCallback)errorCallback {
    return _errorCallback;
}
-(void)setErrorCallback:(IntCallback)newValue {
    _errorCallback = newValue;
}


-(NSMutableURLRequest*)www {
    return _www;
}
-(void)setWww:(NSMutableURLRequest*)newValue {
    _www = newValue;
}

-(int)error {
    return _error;
}
-(void)setError:(int)newValue {
    _error = newValue;
}

-(NSData*)responseData {
    return _responseData;
}
-(void)setResponseData:(NSData*)newValue {
    _responseData = newValue;
}

-(NSDictionary*)responseHeaders {
    return _responseHeaders;
}
-(void)setResponseHeaders:(NSDictionary*)newValue {
    _responseHeaders = newValue;
}

-(id)init {
    self = [super init];
    
    if (self) {
        [self clear];
    }
    return self;
}
-(void)dealloc {
    if ([self verbose]) {
        NSLog(@"Europeana request dealloc");
    }
    [self dispose];
}

-(void)dispose {
    [self clear];
}
-(void)clear {
    [self endSubRequests];
    
    [self setError:0];
    [self setOptions:nil];
    [self setArchiveObjects:nil];
    [self setArchiveObject:nil];
}
-(void)addSubRequest:(EuropeanaRequest*)subRequest {
    if ([self subRequests] == nil) {
        [self setSubRequests:[[NSMutableArray alloc] init]];
    }
    
    [subRequest setOwner:self];
    [subRequest setOptions:[[NSDictionary alloc] initWithDictionary:[self options]]];
    [subRequest setVerbose:[self verbose]];

    if ([subRequest singleCallback] == nil) {
        [subRequest setSingleCallback:[self singleCallback]];
    }
    if ([subRequest totalCallback] == nil) {
        [subRequest setTotalCallback:[self totalCallback]];
    }
    if ([subRequest recordCallback] == nil) {
        [subRequest setRecordCallback:[self recordCallback]];
    }
    if ([subRequest mediaCallback] == nil) {
        [subRequest setMediaCallback:[self mediaCallback]];
    }
    if ([subRequest errorCallback] == nil) {
        [subRequest setErrorCallback:[self errorCallback]];
    }
    
    [[self subRequests] addObject:subRequest];
}
-(void)cancel {
    if ([self verbose]) {
        NSLog(@"Europeana cancelling request");
    }
    
    [self callErrorCallback:[Europeana ERROR_CANCELLED]];
    
    [self end];
}
-(void)end {
    @synchronized(self) {
        [self setEnded:true];
        
        if ([self owner] != nil) {
            [[self owner] removeSubRequest:self];
        }
        [self endSubRequests];
    }
}
-(bool)checkEnded {
    @synchronized(self) {
        if ([self ended]) {
            [self setError: [Europeana ERROR_CANCELLED]];
            IntCallback callback = [self errorCallback];
            if (callback != nil) {
                callback([self error]);
            }
            return true;
        }
        
        return false;
    }
}

-(void)callSingleCallback:(EuropeanaObject*)single {
    @synchronized(self) {
        EuropeanaObjectCallback callback = [self singleCallback];
        if (![self ended] && callback != nil) {
            callback(single);
        }
    }
}
-(void)callTotalCallback:(NSArray*)total {
    @synchronized(self) {
        ArrayCallback callback = [self totalCallback];
        if (![self ended] && callback != nil) {
            callback(total);
        }
    }
}
-(void)callRecordCallback:(EuropeanaObject*)record {
    @synchronized(self) {
        EuropeanaObjectCallback callback = [self recordCallback];
        if (![self ended] && callback != nil) {
            callback(record);
        }
    }
}
-(void)callMediaCallback:(EuropeanaObject*)mediaRecord {
    @synchronized(self) {
        EuropeanaObjectCallback callback = [self mediaCallback];
        if (![self ended] && callback != nil) {
            callback(mediaRecord);
        }
    }
}
-(void)callErrorCallback:(int)error {
    @synchronized(self) {
        IntCallback callback = [self errorCallback];
        if (![self ended] && callback != nil) {
            callback(error);
        }
    }
}
-(void)endSubRequests {
    if ([self subRequests] == nil) {
        return;
    }
    
    NSArray* requests = [[NSArray alloc] initWithArray:[self subRequests]];
    for (EuropeanaRequest* subRequest in requests) {
        [subRequest end];
    }
    
    [self setSubRequests:nil];
}
-(void)removeSubRequest:(EuropeanaRequest*)subRequest {
    if ([self subRequests] == nil) {
        return;
    }
    
    [[self subRequests] removeObject:subRequest];
}

@end

static NSString* EUROPEANA_VALUE_TRUE = @"true";
static NSString* EUROPEANA_VALUE_FALSE = @"false";

static NSString* EUROPEANA_OPTION_ID = @"id";
static NSString* EUROPEANA_OPTION_SEARCH = @"search";
static NSString* EUROPEANA_OPTION_COUNT = @"count";
static NSString* EUROPEANA_OPTION_OFFSET = @"offset";
static NSString* EUROPEANA_OPTION_WIDTH = @"width";
static NSString* EUROPEANA_OPTION_HEIGHT = @"height";
static NSString* EUROPEANA_OPTION_MIN_WIDTH = @"minwidth";
static NSString* EUROPEANA_OPTION_MIN_HEIGHT = @"minheight";
static NSString* EUROPEANA_OPTION_KEEP_ASPECT = @"aspect";
static NSString* EUROPEANA_OPTION_POWER_OF_2 = @"pow2";
static NSString* EUROPEANA_OPTION_CROP_PERCENTAGE = @"crop";
static NSString* EUROPEANA_OPTION_TEXTURE_SIZE = @"textureSize";
static NSString* EUROPEANA_OPTION_VERBOSE_LOGGING = @"verbose";
static NSString* EUROPEANA_OPTION_NO_MEDIA = @"nomedia";

static int EUROPEANA_ERROR_NONE = 0;
static int EUROPEANA_ERROR_NO_RESULTS = 1;
static int EUROPEANA_ERROR_NONE_FOUND = 2;
static int EUROPEANA_ERROR_REQUEST = 3;
static int EUROPEANA_ERROR_BAD_DATA = 4;
static int EUROPEANA_ERROR_NO_TERMS = 5;
static int EUROPEANA_ERROR_NO_TYPE = 6;
static int EUROPEANA_ERROR_CANCELLED = 7;
static int EUROPEANA_ERROR_BAD_ARCHIVE = 8;
static int EUROPEANA_ERROR_RETRIEVING = 9;
static int EUROPEANA_ERROR_MEDIA_REQUEST = 10;

static NSString* EUROPEANA_UNKNOWN_ERROR = @"Unknown error";

static NSString* EUROPEANA_API_URL = @"http://sgiserver.co.uk";
static NSString* EUROPEANA_API_PORT = @"9001";

static NSString* EUROPEANA_PARAM_QUERY = @"?";
static NSString* EUROPEANA_PARAM_SEPERATOR = @"&";
static NSString* EUROPEANA_PARAM_EQUALS = @"=";

static NSString* EUROPEANA_PARAM_TYPE = @"type";
static NSString* EUROPEANA_PARAM_SEARCH = @"search";
static NSString* EUROPEANA_PARAM_FIRST_INDEX = @"first";
static NSString* EUROPEANA_PARAM_COUNT = @"count";
static NSString* EUROPEANA_PARAM_MEDIA = @"media";
static NSString* EUROPEANA_PARAM_ONLY_AVAILABLE = @"availableOnly";
static NSString* EUROPEANA_PARAM_ID = @"id";
static NSString* EUROPEANA_PARAM_WIDTH = @"width";
static NSString* EUROPEANA_PARAM_HEIGHT = @"height";
static NSString* EUROPEANA_PARAM_MIN_WIDTH = @"minwidth";
static NSString* EUROPEANA_PARAM_MIN_HEIGHT = @"minheight";
static NSString* EUROPEANA_PARAM_KEEP_ASPECT = @"aspect";
static NSString* EUROPEANA_PARAM_POWER_OF_2 = @"pow2";
static NSString* EUROPEANA_PARAM_CROP_PERCENTAGE = @"crop";

static NSString* EUROPEANA_TYPE_RECORDS = @"records";
static NSString* EUROPEANA_TYPE_RECORD = @"record";
static NSString* EUROPEANA_TYPE_IMAGE = @"image";
//	static NSString* EUROPEANA_TYPE_AUDIO = @"audio"; // Not ready for Unity
//	static NSString* EUROPEANA_TYPE_VIDEO = @"video"; // Not ready for Unity

static NSString* EUROPEANA_RESULT_ITEMS = @"items";
static NSString* EUROPEANA_RESULT_TITLE = @"title";
static NSString* EUROPEANA_RESULT_DESCRIPTION = @"description";
static NSString* EUROPEANA_RESULT_ATTRIBUTION = @"attribution";
static NSString* EUROPEANA_RESULT_PROVIDER = @"provider";
static NSString* EUROPEANA_RESULT_CREDIT = @"credit";
static NSString* EUROPEANA_RESULT_LIST = @"list";

static NSString* EUROPEANA_RESPONSE_WARNING = @"WARNING";

static int EUROPEANA_DEFAULT_COUNT = 1;
static int EUROPEANA_DEFAULT_OFFSET = 0;
static int EUROPEANA_DEFAULT_TEXTURE_SIZE = -1; //1024;

static int EUROPEANA_NUM_INVALID = -1;

@implementation Europeana

+(NSString*) VALUE_TRUE { return EUROPEANA_VALUE_TRUE; }
+(NSString*) VALUE_FALSE { return EUROPEANA_VALUE_FALSE; }

+(NSString*) OPTION_ID { return EUROPEANA_OPTION_ID; }
+(NSString*) OPTION_SEARCH { return EUROPEANA_OPTION_SEARCH; }
+(NSString*) OPTION_COUNT { return EUROPEANA_OPTION_COUNT; }
+(NSString*) OPTION_OFFSET { return EUROPEANA_OPTION_OFFSET; }
+(NSString*) OPTION_WIDTH { return EUROPEANA_OPTION_WIDTH; }
+(NSString*) OPTION_HEIGHT { return EUROPEANA_OPTION_HEIGHT; }
+(NSString*) OPTION_MIN_WIDTH { return EUROPEANA_OPTION_MIN_WIDTH; }
+(NSString*) OPTION_MIN_HEIGHT { return EUROPEANA_OPTION_MIN_HEIGHT; }
+(NSString*) OPTION_KEEP_ASPECT { return EUROPEANA_OPTION_KEEP_ASPECT; }
+(NSString*) OPTION_POWER_OF_2 { return EUROPEANA_OPTION_POWER_OF_2; }
+(NSString*) OPTION_CROP_PERCENTAGE { return EUROPEANA_OPTION_CROP_PERCENTAGE; }
+(NSString*) OPTION_TEXTURE_SIZE { return EUROPEANA_OPTION_TEXTURE_SIZE; }
+(NSString*) OPTION_VERBOSE_LOGGING { return EUROPEANA_OPTION_VERBOSE_LOGGING; }
+(NSString*) OPTION_NO_MEDIA { return EUROPEANA_OPTION_NO_MEDIA; }

+(int) ERROR_NONE { return EUROPEANA_ERROR_NONE; }
+(int) ERROR_NO_RESULTS { return EUROPEANA_ERROR_NO_RESULTS; }
+(int) ERROR_NONE_FOUND { return EUROPEANA_ERROR_NONE_FOUND; }
+(int) ERROR_REQUEST { return EUROPEANA_ERROR_REQUEST; }
+(int) ERROR_BAD_DATA { return EUROPEANA_ERROR_BAD_DATA; }
+(int) ERROR_NO_TERMS { return EUROPEANA_ERROR_NO_TERMS; }
+(int) ERROR_NO_TYPE { return EUROPEANA_ERROR_NO_TYPE; }
+(int) ERROR_CANCELLED { return EUROPEANA_ERROR_CANCELLED; }
+(int) ERROR_BAD_ARCHIVE { return EUROPEANA_ERROR_BAD_ARCHIVE; }
+(int) ERROR_RETRIEVING { return EUROPEANA_ERROR_RETRIEVING; }
+(int) ERROR_MEDIA_REQUEST { return EUROPEANA_ERROR_MEDIA_REQUEST; }

+(NSArray*) ERRORS {
    return [[NSArray alloc] initWithObjects: @"No error found",
                                             @"No results from specified parameters and search terms, offset may be too great",
                                             @"No results after filtering for available assets, offset may be too great",
                                             @"Problem sending request, search terms may include invalid characters",
                                             @"Malformed or empty data received from server, may be inaccessible",
                                             @"No or bad search terms",
                                             @"No media type specified, media type necessary to retrieve content",
                                             @"Cancelled by user",
                                             @"The content could not be retrieved from the archive that provides this asset",
                                             @"There was an error retrieving the asset from an archive entry page",
                                             @"Problem sending media request, catching an error with WWW request - use verbose logging to check URL being sent", nil];
}

+(NSString*) UNKNOWN_ERROR { return EUROPEANA_UNKNOWN_ERROR; }

+(NSString*) API_URL { return EUROPEANA_API_URL; }
+(NSString*) API_PORT { return EUROPEANA_API_PORT; }

+(NSString*) PARAM_QUERY { return EUROPEANA_PARAM_QUERY; }
+(NSString*) PARAM_SEPERATOR { return EUROPEANA_PARAM_SEPERATOR; }
+(NSString*) PARAM_EQUALS { return EUROPEANA_PARAM_EQUALS; }

+(NSString*) PARAM_TYPE { return EUROPEANA_PARAM_TYPE; }
+(NSString*) PARAM_SEARCH { return EUROPEANA_PARAM_SEARCH; }
+(NSString*) PARAM_FIRST_INDEX { return EUROPEANA_PARAM_FIRST_INDEX; }
+(NSString*) PARAM_COUNT { return EUROPEANA_PARAM_COUNT; }
+(NSString*) PARAM_MEDIA { return EUROPEANA_PARAM_MEDIA; }
+(NSString*) PARAM_ONLY_AVAILABLE { return EUROPEANA_PARAM_ONLY_AVAILABLE; }
+(NSString*) PARAM_ID { return EUROPEANA_PARAM_ID; }
+(NSString*) PARAM_WIDTH { return EUROPEANA_PARAM_WIDTH; }
+(NSString*) PARAM_HEIGHT { return EUROPEANA_PARAM_HEIGHT; }
+(NSString*) PARAM_MIN_WIDTH { return EUROPEANA_PARAM_MIN_WIDTH; }
+(NSString*) PARAM_MIN_HEIGHT { return EUROPEANA_PARAM_MIN_HEIGHT; }
+(NSString*) PARAM_KEEP_ASPECT { return EUROPEANA_PARAM_KEEP_ASPECT; }
+(NSString*) PARAM_POWER_OF_2 { return EUROPEANA_PARAM_POWER_OF_2; }
+(NSString*) PARAM_CROP_PERCENTAGE { return EUROPEANA_PARAM_CROP_PERCENTAGE; }

+(NSString*) TYPE_RECORDS { return EUROPEANA_TYPE_RECORDS; }
+(NSString*) TYPE_RECORD { return EUROPEANA_TYPE_RECORD; }
+(NSString*) TYPE_IMAGE { return EUROPEANA_TYPE_IMAGE; }
//    +(NSString*) TYPE_AUDIO { return EUROPEANA_TYPE_AUDIO; } // Not ready for iOS
//    +(NSString*) TYPE_VIDEO { return EUROPEANA_TYPE_VIDEO; } // Not ready for iOS

+(NSString*) RESULT_ITEMS { return EUROPEANA_RESULT_ITEMS; }
+(NSString*) RESULT_TITLE { return EUROPEANA_RESULT_TITLE; }
+(NSString*) RESULT_DESCRIPTION { return EUROPEANA_RESULT_DESCRIPTION; }
+(NSString*) RESULT_ATTRIBUTION { return EUROPEANA_RESULT_ATTRIBUTION; }
+(NSString*) RESULT_PROVIDER { return EUROPEANA_RESULT_PROVIDER; }
+(NSString*) RESULT_CREDIT { return EUROPEANA_RESULT_CREDIT; }
+(NSString*) RESULT_LIST { return EUROPEANA_RESULT_LIST; }

+(NSString*) RESPONSE_WARNING { return EUROPEANA_RESPONSE_WARNING; }

+(int) DEFAULT_COUNT { return EUROPEANA_DEFAULT_COUNT; }
+(int) DEFAULT_OFFSET { return EUROPEANA_DEFAULT_OFFSET; }
+(int) DEFAULT_TEXTURE_SIZE { return EUROPEANA_DEFAULT_TEXTURE_SIZE; }
+(NSString*) DEFAULT_VERBOSE { return EUROPEANA_VALUE_FALSE; }

+(int) NUM_INVALID { return EUROPEANA_NUM_INVALID; }


+(NSString*) PrintError:(int)error {
    NSString* message = nil;
    if (error <= [Europeana NUM_INVALID] || error >= [[Europeana ERRORS] count]) {
        message = [Europeana UNKNOWN_ERROR];
    } else {
        message = [[Europeana ERRORS] objectAtIndex:error];
    }
    NSLog(@"%@", message);
    return message;
}

+(EuropeanaRequest*) GetImages:(NSDictionary*)options withSingle:(EuropeanaObjectCallback)single andTotal:(void(^)(NSArray*))total andError:(IntCallback)error {
    
    return [Europeana GetRecords:[Europeana TYPE_IMAGE] options:options single:single total:total error:error];
}

+(EuropeanaRequest*) GetImages:(NSString*)search count:(int)count offset:(int)offset width:(int)width height:(int)height keepAspect:(bool)keepAspect powerOf2:(bool)powerOf2 cropPercentage:(int)cropPercentage textureSize:(int)textureSize single:(EuropeanaObjectCallback)single total:(ArrayCallback)total error:(IntCallback)error {
    
    NSMutableDictionary* options = [[NSMutableDictionary alloc] init];
    [options setObject:search forKey:[Europeana OPTION_SEARCH]];
    [options setObject:[NSString stringWithFormat:@"%i", count] forKey:[Europeana OPTION_COUNT]];
    [options setObject:[NSString stringWithFormat:@"%i", offset] forKey:[Europeana OPTION_OFFSET]];
    [options setObject:[NSString stringWithFormat:@"%i", width] forKey:[Europeana OPTION_WIDTH]];
    [options setObject:[NSString stringWithFormat:@"%i", height] forKey:[Europeana OPTION_HEIGHT]];
    [options setObject:keepAspect ? [Europeana VALUE_TRUE] : [Europeana VALUE_FALSE] forKey:[Europeana OPTION_KEEP_ASPECT]];
    [options setObject:powerOf2 ? [Europeana VALUE_TRUE] : [Europeana VALUE_FALSE] forKey:[Europeana OPTION_POWER_OF_2]];
    [options setObject:[NSString stringWithFormat:@"%i", cropPercentage] forKey:[Europeana OPTION_CROP_PERCENTAGE]];
    [options setObject:[NSString stringWithFormat:@"%i", textureSize] forKey:[Europeana OPTION_TEXTURE_SIZE]];
    [options setObject:[Europeana VALUE_FALSE] forKey:[Europeana OPTION_NO_MEDIA]];
    
    return [Europeana GetImages:options withSingle:single andTotal:total andError:error];
}

+(EuropeanaRequest*) GetImage:(NSDictionary*)options withCallback:(EuropeanaObjectCallback)callback andError:(IntCallback)error {

    NSString* recordId = [options objectForKey:[Europeana OPTION_ID]];
    EuropeanaRequest* request = [[EuropeanaRequest alloc] init];
    [request setErrorCallback:error];
    [request setOptions:[[NSDictionary alloc] initWithDictionary:options copyItems:true]];

    [Europeana SetVerboseLoggingForRequest:request withOptions:options];
    
    return [Europeana GetRecord:recordId type:[Europeana TYPE_IMAGE] request:request callback:callback];
}

+(EuropeanaRequest*) GetImage:(NSString*)recordId width:(int)width height:(int)height keepAspect:(bool)keepAspect powerOf2:(bool)powerOf2 cropPercentage:(int)cropPercentage textureSize:(int)textureSize callback:(EuropeanaObjectCallback)callback error:(IntCallback)error {
    
    NSMutableDictionary* options = [[NSMutableDictionary alloc] init];
    [options setObject:recordId forKey:[Europeana OPTION_ID]];
    [options setObject:[NSString stringWithFormat:@"%i", width] forKey:[Europeana OPTION_WIDTH]];
    [options setObject:[NSString stringWithFormat:@"%i", height] forKey:[Europeana OPTION_HEIGHT]];
    [options setObject:keepAspect ? [Europeana VALUE_TRUE] : [Europeana VALUE_FALSE] forKey:[Europeana OPTION_KEEP_ASPECT]];
    [options setObject:powerOf2 ? [Europeana VALUE_TRUE] : [Europeana VALUE_FALSE] forKey:[Europeana OPTION_POWER_OF_2]];
    [options setObject:[NSString stringWithFormat:@"%i", cropPercentage] forKey:[Europeana OPTION_CROP_PERCENTAGE]];
    [options setObject:[NSString stringWithFormat:@"%i", textureSize] forKey:[Europeana OPTION_TEXTURE_SIZE]];
    
    return [Europeana GetImage:options withCallback:callback andError:error];
}

+(void)SetVerboseLoggingForRequest:(EuropeanaRequest*)request withOptions:(NSDictionary*)options {
    NSString* isVerbose = [Europeana DEFAULT_VERBOSE];
    @try {
        isVerbose = [[request options] objectForKey:[Europeana OPTION_VERBOSE_LOGGING]];
    } @catch (NSException* exception) {
        isVerbose = [Europeana DEFAULT_VERBOSE];
    }
    
    if ([isVerbose caseInsensitiveCompare:[Europeana VALUE_FALSE]] == NSOrderedSame) {
        [request setVerbose:false];
    } else {
        [request setVerbose:true];
    }
}

+(EuropeanaRequest*) GetRecords:(NSString*)type options:(NSDictionary*)options single:(EuropeanaObjectCallback)single total:(ArrayCallback)total error:(IntCallback)error {
    
    EuropeanaRequest* request = [[EuropeanaRequest alloc] init];
    [request setSingleCallback:single];
    [request setTotalCallback:total];
    [request setErrorCallback:error];
    [request setOptions:[[NSDictionary alloc] initWithDictionary:options copyItems:true]];
    
    [Europeana SetVerboseLoggingForRequest:request withOptions:options];
    
    [Europeana DoGetRecords:type request:request];
    return request;
}

+(void) DoGetRecords:(NSString*)type request:(EuropeanaRequest*)request {
    
    if ([request checkEnded]) {
        return;
    }
    
    if ([request verbose]) NSLog(@"Europeana getting records with type %@", type);

    if (type == nil) {
        [request setSingleCallback:nil];
        
        if ([request verbose]) NSLog(@"Europeana type not specified");
        
        if ([request errorCallback] != nil) {
            [request setError:[Europeana ERROR_NO_TYPE]];
            [request callErrorCallback:[request error]];
        } else if ([request totalCallback] != nil) {
            [request setArchiveObjects:[[NSArray alloc] init]];
            [request callTotalCallback:[request archiveObjects]];
        }
        [request end];
        return;
    }
    
    type = [type lowercaseString];
    
    NSString* search = nil;
    @try {
        search = [[request options] objectForKey:[Europeana OPTION_SEARCH]];
    } @catch (NSException* exception) {
        search = nil;
    }
    
    if (search == nil) {
        if ([request verbose]) NSLog(@"Europeana search option not provided!");

        if ([request errorCallback] != nil) {
            [request setError:[Europeana ERROR_NO_TERMS]];
            [request callErrorCallback:[request error]];
        } else if ([request totalCallback] != nil) {
            [request setArchiveObjects:[[NSArray alloc] init]];
            [request callTotalCallback:[request archiveObjects]];
        }
        [request end];
        return;
    }
    
    NSString* count = [NSString stringWithFormat:@"%i", [Europeana DEFAULT_COUNT]];
    @try {
        count = [[request options] objectForKey:[Europeana OPTION_COUNT]];
    } @catch (NSException* exception) {
        count = [NSString stringWithFormat:@"%i", [Europeana DEFAULT_COUNT]];
    }
    
    NSString* offset = [NSString stringWithFormat:@"%i", [Europeana DEFAULT_OFFSET]];
    @try {
        offset = [[request options] objectForKey:[Europeana OPTION_OFFSET]];
    } @catch (NSException* exception) {
        offset = [NSString stringWithFormat:@"%i", [Europeana DEFAULT_OFFSET]];
    }
    
    NSMutableString* url = [NSMutableString stringWithFormat:@"%@:%@/", [Europeana API_URL], [Europeana API_PORT]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_QUERY], [Europeana PARAM_TYPE], [Europeana PARAM_EQUALS], [Europeana TYPE_RECORDS]]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_SEARCH], [Europeana PARAM_EQUALS], [search stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_MEDIA], [Europeana PARAM_EQUALS], type]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_COUNT], [Europeana PARAM_EQUALS], count]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_FIRST_INDEX], [Europeana PARAM_EQUALS], offset]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_ONLY_AVAILABLE], [Europeana PARAM_EQUALS], [Europeana VALUE_TRUE]]];
    
    NSString* minWidth = nil;
    @try {
        minWidth = [[request options] objectForKey:[Europeana OPTION_MIN_WIDTH]];
    } @catch (NSException* exception) {
        minWidth = nil;
    }
    if (minWidth != nil && [minWidth intValue] > -1) {
        [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_MIN_WIDTH], [Europeana PARAM_EQUALS], minWidth]];
    }

    NSString* minHeight = nil;
    @try {
        minHeight = [[request options] objectForKey:[Europeana OPTION_MIN_HEIGHT]];
    } @catch (NSException* exception) {
        minHeight = nil;
    }
    if (minHeight != nil && [minHeight intValue] > -1) {
        [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_MIN_HEIGHT], [Europeana PARAM_EQUALS], minHeight]];
    }
    
    if ([request verbose]) NSLog(@"Europeana records query Url: %@", url);
    
    NSData* wwwData = [Europeana RetrieveDataFromURL:url withRequest:request];
    if (wwwData == nil) {
        if ([request errorCallback] != nil) {
            [request setError:[Europeana ERROR_REQUEST]];
            [request callErrorCallback:[request error]];
        } else if ([request totalCallback] != nil) {
            [request setArchiveObjects:[[NSArray alloc] init]];
            [request callTotalCallback:[request archiveObjects]];
        }
        [request end];
        return;
    }
    
    NSMutableArray* ids = [[NSMutableArray alloc] init];
    
    if ([request www] != nil) {
        if ([request checkEnded]) {
            return;
        }
        
        if ([Europeana BadWWWResponse:request]) {
            return;
        }
        
        NSDictionary* json = nil;
        NSArray* jsonList = nil;
        NSError* jsonError = nil;

        @try {
            json = [NSJSONSerialization JSONObjectWithData:wwwData options: NSJSONReadingMutableContainers error:&jsonError];
            jsonList = [json objectForKey:[Europeana RESULT_ITEMS]];
        } @catch (NSException* exception) {
            if ([request errorCallback] != nil) {
                [request setError:[Europeana ERROR_BAD_DATA]];
                [request callErrorCallback:[request error]];
            } else if ([request totalCallback] != nil) {
                [request setArchiveObjects:[[NSArray alloc] init]];
                [request callTotalCallback:[request archiveObjects]];
            }
            [request end];
            return;
        }
        
        if (jsonList != nil) {
            for (int i = 0; i < [jsonList count]; i++) {
                NSString* recordId = [jsonList objectAtIndex:i];
                [ids addObject:recordId];
            }
        }
    }
    
    if ([ids count] <= 0) {
        if ([request verbose]) NSLog(@"Europeana no results found with specified parameters and search terms: %@", search);

        if ([request errorCallback] != nil) {
            [request setError:[Europeana ERROR_NO_RESULTS]];
            [request callErrorCallback:[request error]];
        } else if ([request totalCallback] != nil) {
            [request setArchiveObjects:[[NSArray alloc] init]];
            [request callTotalCallback:[request archiveObjects]];
        }
        [request end];
        return;
    }
    
    [Europeana IterateRecords:ids index:-1 type:type objects:nil request:request];
}

+(void) IterateRecords:(NSArray*)items index:(int)index type:(NSString*)type objects:(NSMutableArray*)objects request:(EuropeanaRequest*)request {

    if ([request checkEnded]) {
        return;
    }
    
    if (objects == nil) {
        objects = [[NSMutableArray alloc] init];
    }
    
    index++;
    if (index >= [items count]) {
        if ([items count] <= 0 && [request errorCallback] != nil) {
            [request setError:[Europeana ERROR_NONE_FOUND]];
            [request callErrorCallback:[request error]];
        } else if ([request totalCallback] != nil) {
            [request setArchiveObjects:objects];
            [request callTotalCallback:[request archiveObjects]];
        }
        [request end];
        return;
    }
    
    NSString* item = [items objectAtIndex:index];
    
    [Europeana GetRecord:item type:type request:request callback:^(EuropeanaObject* newObject){
        if ([request checkEnded]) {
            return;
        }
        
        if (newObject != nil) {
            [newObject setIndex:(int)[objects count]];
            [objects addObject:newObject];
            [request callSingleCallback:newObject];
        }
        
        [Europeana IterateRecords:items index:index type:type objects:objects request:request];
    }];
}

+(EuropeanaRequest*) GetRecord:(NSString*)recordId type:(NSString*)type request:(EuropeanaRequest*)request callback:(EuropeanaObjectCallback)callback {
    if (request != nil && [request checkEnded]) {
        return nil;
    }
    
    EuropeanaRequest* subRequest = [[EuropeanaRequest alloc] init];
    [subRequest setRecordCallback:callback];
    if (request != nil) {
        [request addSubRequest:subRequest];
    }
    
    [Europeana DoGetRecord:recordId type:type request:subRequest];
    return subRequest;
}

+(EuropeanaRequest*) GetRecordWithOptions:(NSDictionary*)options recordId:(NSString*)recordId type:(NSString*)type request:(EuropeanaRequest*)request callback:(EuropeanaObjectCallback)callback {
    if (request != nil && [request checkEnded]) {
        return nil;
    }
    
    EuropeanaRequest* subRequest = [[EuropeanaRequest alloc] init];
    [subRequest setRecordCallback:callback];
    [subRequest setOptions:options];
    if (request != nil) {
        [request addSubRequest:subRequest];
    }
    
    [Europeana DoGetRecord:recordId type:type request:subRequest];
    return subRequest;
}

+(void) DoGetRecord:(NSString*)recordId type:(NSString*)type request:(EuropeanaRequest*)request {
    if ([request checkEnded]) {
        return;
    }
    
    NSMutableString* url = [NSMutableString stringWithFormat:@"%@:%@/", [Europeana API_URL], [Europeana API_PORT]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_QUERY], [Europeana PARAM_TYPE], [Europeana PARAM_EQUALS], [Europeana TYPE_RECORD]]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_ID], [Europeana PARAM_EQUALS], recordId]];

    if ([request verbose]) NSLog(@"Europeana record query Url: %@", url);

    NSData* wwwData = [Europeana RetrieveDataFromURL:url withRequest:request];
    if (wwwData == nil) {
        [request callRecordCallback:nil];
        [request end];
        return;
    }
    
    if ([request checkEnded]) {
        return;
    }
    
    if ([Europeana BadWWWResponse:request]) {
        [request callRecordCallback:nil];
        return;
    }
    
    NSDictionary* json = nil;
    NSError* jsonError = nil;
    
    @try {
        json = [NSJSONSerialization JSONObjectWithData:wwwData options:NSJSONReadingMutableContainers error:&jsonError];
    } @catch (NSException* exception) {
        [request callRecordCallback:nil];
        [request end];
        return;
    }
    
    [request setArchiveObject:[[EuropeanaObject alloc] initWithId:recordId andType:type]];
    
    @try {
        [[request archiveObject] setTitle:[json objectForKey:[Europeana RESULT_TITLE]]];
    } @catch (NSException* exception) {
    }
    
    @try {
        [[request archiveObject] setDescription:[json objectForKey:[Europeana RESULT_DESCRIPTION]]];
    } @catch (NSException* exception) {
    }

    NSDictionary* attribution = nil;

    @try {
       attribution = [json objectForKey:[Europeana RESULT_ATTRIBUTION]];
    } @catch (NSException* exception) {
    }
    
    if (attribution != nil) {
        @try {
            [[request archiveObject] setProvider:[attribution objectForKey:[Europeana RESULT_PROVIDER]]];
        } @catch (NSException* exception) {
            [[request archiveObject] setProvider:nil];
        }

        @try {
            [[request archiveObject] setCredit:[attribution objectForKey:[Europeana RESULT_CREDIT]]];
        } @catch (NSException* exception) {
            [[request archiveObject] setCredit:nil];
        }
    }
    
    if ([[[request options] objectForKey:[Europeana OPTION_NO_MEDIA]] caseInsensitiveCompare:[Europeana VALUE_TRUE]] == NSOrderedSame) {
        EuropeanaObject* archiveObject = [request archiveObject];
        [request callRecordCallback:archiveObject];
    } else {
        [Europeana GetMediaForRecord:recordId type:type recordObject:[request archiveObject] request:request callback:^(EuropeanaObject* successObject){
            if ([request checkEnded]) {
                return;
            }
            
            [request callRecordCallback:successObject];
        }];
    }
}

+(EuropeanaRequest*) GetMediaForRecord:(NSString*)recordId type:(NSString*)type recordObject:(EuropeanaObject*)recordObject request:(EuropeanaRequest*)request callback:(EuropeanaObjectCallback)callback {
    
    if ([request checkEnded]) {
        return nil;
    }
    
    EuropeanaRequest* subRequest = [[EuropeanaRequest alloc] init];
    [subRequest setMediaCallback:callback];
    [request addSubRequest:subRequest];
    
    [Europeana DoGetMediaForRecord:recordId type:type recordObject:recordObject request:subRequest];
    return subRequest;
}

+(void) DoGetMediaForRecord:(NSString*)recordId type:(NSString*)type recordObject:(EuropeanaObject*)recordObject request:(EuropeanaRequest*)request {
    
    if ([request checkEnded]) {
        return;
    }

    NSMutableString* url = [NSMutableString stringWithFormat:@"%@:%@/", [Europeana API_URL], [Europeana API_PORT]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_QUERY], [Europeana PARAM_TYPE], [Europeana PARAM_EQUALS], type]];
    [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_ID], [Europeana PARAM_EQUALS], recordId]];
    
    if ([type caseInsensitiveCompare:[Europeana TYPE_IMAGE]] == NSOrderedSame) {
        @try {
            NSString* width = [[request options] objectForKey:[Europeana OPTION_WIDTH]];
            if ([width intValue] > 0) {
                [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_WIDTH], [Europeana PARAM_EQUALS], width]];
            }
        } @catch (NSException* exception) {
            if ([request verbose]) NSLog(@"Europeana getting image, width not supplied");
        }

        @try {
            NSString* height = [[request options] objectForKey:[Europeana OPTION_HEIGHT]];
            if ([height intValue] > 0) {
                [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_HEIGHT], [Europeana PARAM_EQUALS], height]];
            }
        } @catch (NSException* exception) {
            if ([request verbose]) NSLog(@"Europeana getting image, height not supplied");
        }

        @try {
            NSString* aspect = [[request options] objectForKey:[Europeana OPTION_KEEP_ASPECT]];
            NSString* keepAspect = [Europeana VALUE_FALSE];
            if ([aspect caseInsensitiveCompare:[Europeana VALUE_TRUE]] == NSOrderedSame) {
                keepAspect = [Europeana VALUE_TRUE];
            }
            [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_KEEP_ASPECT], [Europeana PARAM_EQUALS], keepAspect]];
        } @catch (NSException* exception) {
            if ([request verbose]) NSLog(@"Europeana getting image, keep aspect ratio not supplied");
        }

        @try {
            NSString* pow2 = [[request options] objectForKey:[Europeana OPTION_POWER_OF_2]];
            NSString* powerOf2 = [Europeana VALUE_FALSE];
            if ([pow2 caseInsensitiveCompare:[Europeana VALUE_TRUE]] == NSOrderedSame) {
                powerOf2 = [Europeana VALUE_TRUE];
            }
            [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_POWER_OF_2], [Europeana PARAM_EQUALS], powerOf2]];
        } @catch (NSException* exception) {
            if ([request verbose]) NSLog(@"Europeana getting image, power of 2 not supplied");
        }
        
        @try {
            NSString* crop = [[request options] objectForKey:[Europeana OPTION_CROP_PERCENTAGE]];
            if ([crop intValue] > 0) {
                [url appendString:[NSString stringWithFormat:@"%@%@%@%@", [Europeana PARAM_SEPERATOR], [Europeana PARAM_CROP_PERCENTAGE], [Europeana PARAM_EQUALS], crop]];
            }
        } @catch (NSException* exception) {
            if ([request verbose]) NSLog(@"Europeana getting image, crop percentage not supplied");
        }
    } else {
        if ([request verbose]) NSLog(@"Europeana media type %@ not implemented", type);

        [request callMediaCallback:nil];
        [request end];
        return;
    }
    
    if ([request verbose]) NSLog(@"Europeana getting media Url: %@", url);
    
    NSData* wwwData = [Europeana RetrieveDataFromURL:url withRequest:request];
    if (wwwData == nil) {
        [request callMediaCallback:nil];
        if ([request errorCallback] != nil) {
            [request setError:[Europeana ERROR_MEDIA_REQUEST]];
            [request callErrorCallback:[request error]];
        }
        [request end];
        return;
    }
    
    if ([request checkEnded]) {
        return;
    }
    
    if ([Europeana BadWWWResponse:request]) {
        [request callMediaCallback:nil];
        return;
    }
    
    if ([type caseInsensitiveCompare:[Europeana TYPE_IMAGE]] == NSOrderedSame) {
        int size = [Europeana DEFAULT_TEXTURE_SIZE];
        
        @try {
            int testSize = [[[request options] objectForKey:[Europeana OPTION_TEXTURE_SIZE]] intValue];
            if (testSize > 0) {
                size = testSize;
            }
        } @catch (NSException* exception) {
            size = [Europeana DEFAULT_TEXTURE_SIZE];
        }
        
        UIImage* texture = nil;
        
        @try {
            texture = [UIImage imageWithData:wwwData];
        } @catch (NSException* exception) {
            texture = nil;
        }
        
        if (texture != nil && size != [Europeana DEFAULT_TEXTURE_SIZE]) {
            @try {
                CGSize imageSize = CGSizeMake(size, size);
                
                if ([Europeana DeviceIsRetina])
                    UIGraphicsBeginImageContextWithOptions(imageSize, NO, 2.0);
                else
                    UIGraphicsBeginImageContext(imageSize);
                
                CGContextRef context = UIGraphicsGetCurrentContext();
                
                CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
                CGContextFillRect(context, CGRectMake(0.0, 0.0, size, size));
                
                CGRect imageRect = CGRectMake((size-texture.size.width)/2, (size-texture.size.height)/2, texture.size.width, texture.size.height);
                
                [texture drawInRect:imageRect];
                
                UIImage * resizedImage = UIGraphicsGetImageFromCurrentImageContext();
                resizedImage = [resizedImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
                
                UIGraphicsEndImageContext();
                
                if (resizedImage != nil) {
                    texture = resizedImage;
                }
            } @catch (NSException* exception) {
            }
        }
        
        if (texture != nil) {
            [recordObject setTextureData:texture];
            [request setArchiveObject:recordObject];
            [request callMediaCallback:recordObject];
            [request end];
        } else {
            [request callMediaCallback:nil];
            [request end];
        }
    } else {
        if ([request verbose]) NSLog(@"Europeana media type %@ not implemented", type);
        
        [request setArchiveObject:recordObject];
        [request callMediaCallback:recordObject];
        [request end];
    }
}

+(bool) BadWWWResponse:(EuropeanaRequest*)request {
    int warning = [Europeana ERROR_NONE];
    
    @try {
        warning = [[[request responseHeaders] objectForKey:[Europeana RESPONSE_WARNING]] intValue];
    } @catch (NSException* exception) {
        warning = [Europeana ERROR_NONE];
    }
    
    if (warning != [Europeana ERROR_NONE]) {
        if ([request verbose]) {
            NSString* message = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];
            NSLog(@"Europeana response code: %@, message: %@", [NSString stringWithFormat:@"%i", warning], message);
        }
        
        [request setError:warning];
        [request callErrorCallback:[request error]];
        [request end];
        return true;
    }
    return false;
}

+(NSData*) RetrieveDataFromURL:(NSString*)url withRequest:(EuropeanaRequest*)request {
    NSMutableURLRequest* www = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                       cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                   timeoutInterval:60];
    [request setWww:www];

    [www setHTTPMethod: @"GET"];

    NSError* wwwError;
    NSHTTPURLResponse* wwwResponse = nil;

    NSData* wwwData = nil;

    @try {
        wwwData = [NSURLConnection sendSynchronousRequest:www returningResponse:&wwwResponse error:&wwwError];
        
        if (wwwError != nil) {
            if ([request verbose]) NSLog(@"Europeana error getting WWW data, code: %ld message: %@", (long)wwwError.code, [wwwError.userInfo objectForKey:@"NSLocalizedDescription"]);
        }
        
        if ([wwwResponse respondsToSelector:@selector(allHeaderFields)]) {
            NSDictionary* responseHeaders = [wwwResponse allHeaderFields];
            [request setResponseHeaders:responseHeaders];
        }
    } @catch (NSException* exception) {
        wwwData = nil;

        if ([request verbose]) NSLog(@"Europeana exception getting WWW data, name: %@ message: %@", exception.name, exception.reason);
    }
    
    if (wwwData != nil) {
        [request setResponseData:wwwData];
        
        if ([request verbose]) {
            NSString* dataString = [[NSString alloc] initWithData:wwwData encoding:NSUTF8StringEncoding];
            NSLog(@"Europeana retrieved data: %@", dataString);
        }
    }
    
    return wwwData;
}

+(bool) DeviceIsRetina {
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        if ([[UIScreen mainScreen] scale] == 2.0) {
            return true;
        }
    }
    return false;
}

@end
