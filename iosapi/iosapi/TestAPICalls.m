//
//  TestAPICalls.m
//  iosapi
//
//  Created by Terry Goodwin on 03/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TestAPICalls.h"

@implementation TestAPICalls

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self start];
}

-(EuropeanaRequest*)myRequest {
    return _myRequest;
}
-(void)setMyRequest:(EuropeanaRequest*)newValue {
    _myRequest = newValue;
}

-(void)start {
    NSString* searchTerms = @"classical painting";
    NSString* singleId = @"/90402/SK_A_190";
    NSString* offset = @"0";
    NSString* width = @"640";
    NSString* height = @"400";
    NSString* minWidth = @"256";
    NSString* minHeight = minWidth;
    NSString* keepAspect = [Europeana VALUE_TRUE];
    NSString* pow2 = [Europeana VALUE_TRUE];
    NSString* cropPercentage = @"10";
    NSString* textureSize = @"1024";
    NSString* verboseLogging = [Europeana VALUE_TRUE];
    
    NSLog(@"Retrieving Europeana images with terms: %@", searchTerms);
    
    EuropeanaRequest* newRequest = [Europeana GetImages:[[NSDictionary alloc] initWithObjectsAndKeys:
                                                         searchTerms, [Europeana OPTION_SEARCH],
                                                         [NSString stringWithFormat:@"%lu", (unsigned long)[paintings count]], [Europeana OPTION_COUNT],
                                                         offset, [Europeana OPTION_OFFSET],
                                                         width, [Europeana OPTION_WIDTH],
                                                         height, [Europeana OPTION_HEIGHT],
                                                         minWidth, [Europeana OPTION_MIN_WIDTH],
                                                         minHeight, [Europeana OPTION_MIN_HEIGHT],
                                                         keepAspect, [Europeana OPTION_KEEP_ASPECT],
                                                         pow2, [Europeana OPTION_POWER_OF_2],
                                                         cropPercentage, [Europeana OPTION_CROP_PERCENTAGE],
                                                         textureSize, [Europeana OPTION_TEXTURE_SIZE],
                                                         verboseLogging, [Europeana OPTION_VERBOSE_LOGGING],
                                                         nil]
    withSingle:^(EuropeanaObject* image) {
        NSLog(@"Retrieved image: %@ index: %@", [image objectId], [NSString stringWithFormat:@"%i", [image index]]);
        
        if ([image index] >= [paintings count]) {
            return;
        }
        
        UIImageView* painting = [paintings objectAtIndex:[image index]];
        UILabel* label = [labels objectAtIndex:[image index]];
        
        NSMutableString* newLabel = [[NSMutableString alloc] initWithString:@"No title or attribution data"];
        if ([image title] != nil) {
            newLabel = [[NSMutableString alloc] initWithString:[image title]];
            if ([image attribution] != nil) {
                [newLabel appendString:@"\n"];
            }
        }
        if ([image attribution] != nil) {
            [newLabel appendString:[image attribution]];
        }
        
        [label setText:newLabel];
        
        @try {
            [painting setImage:[image textureData]];
        } @catch (NSException* exception) {
            NSLog(@"Error assigning texture for image %@", [image objectId]);
        }
        
    } andTotal:^(NSArray* images) {
        NSLog(@"Finished retrieving images: %@", [NSString stringWithFormat:@"%lu", (unsigned long)[images count]]);
    } andError:^(int error) {
        NSLog(@"Got an error message! It follows:");
        [Europeana PrintError:error];
    }];
    
    [self setMyRequest:newRequest];
    
    NSLog(@"Retrieving Europeana image with id: %@", singleId);
    
    [Europeana GetImage:[[NSDictionary alloc] initWithObjectsAndKeys:
                         singleId, [Europeana OPTION_ID],
                         width, [Europeana OPTION_WIDTH],
                         height, [Europeana OPTION_HEIGHT],
                         keepAspect, [Europeana OPTION_KEEP_ASPECT],
                         pow2, [Europeana OPTION_POWER_OF_2],
                         cropPercentage, [Europeana OPTION_CROP_PERCENTAGE],
                         textureSize, [Europeana OPTION_TEXTURE_SIZE],
                         verboseLogging, [Europeana OPTION_VERBOSE_LOGGING],
                         nil]
    withCallback:^(EuropeanaObject* image) {
        NSLog(@"Retrieved image: %@", [image objectId]);

        UIImageView* painting = singlePainting;
        UILabel* label = singleLabel;

        NSMutableString* newLabel = [[NSMutableString alloc] initWithString:@"No title or attribution data"];
        if ([image title] != nil) {
            newLabel = [[NSMutableString alloc] initWithString:[image title]];
            if ([image attribution] != nil) {
                [newLabel appendString:@"\n"];
            }
        }
        if ([image attribution] != nil) {
            [newLabel appendString:[image attribution]];
        }

        [label setText:newLabel];

        @try {
            [painting setImage:[image textureData]];
        } @catch (NSException* exception) {
            NSLog(@"Error assigning texture for image %@", [image objectId]);
        }
    } andError:^(int error) {
        NSLog(@"Got an error message! It follows:");
        [Europeana PrintError:error];
    }];
}

@end
