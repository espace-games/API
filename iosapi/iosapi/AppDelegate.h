//
//  AppDelegate.h
//  iosapi
//
//  Created by Terry Goodwin on 01/12/2014.
//  Copyright (c) 2014 Serious Games International Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

