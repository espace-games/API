var MyConsole = function() {
	this.version = new Object;
	this.version.major = "1";
	this.version.minor = "0";
	this.version.string = this.version.major+"."+this.version.minor;

	this.logEnabled = true;
	this.alertsEnabled = true;
	return this;
};
	
MyConsole.prototype.Log = function(message) {
	if (!myConsole.logEnabled) {
		return;
	}
	
	console.log(message);
};

MyConsole.prototype.Alert = function(message) {
	if (!myConsole.alertsEnabled) {
		return;
	}
	
	alert(message);
};
	
var myConsole = new MyConsole();
