var UtilsManager = function() {
	this.version = new Object;
	this.version.major = "1";
	this.version.minor = "03";
	this.version.string = this.version.major+"."+this.version.minor;
	return this;
};

UtilsManager.prototype.DateFromISO = function(s) {
		var day, tz,
		rx=/^(\d{4}\-\d\d\-\d\d([tT ][\d:\.]*)?)([zZ]|([+\-])(\d\d):(\d\d))?$/,
		p= rx.exec(s) || [];
		if(p[1]){
				day= p[1].split(/\D/);
				for(var i= 0, L= day.length; i<L; i++){
						day[i]= parseInt(day[i], 10) || 0;
				};
				day[1]-= 1;
				day= new Date(Date.UTC.apply(Date, day));
				if(!day.getDate()) return NaN;
				if(p[5]){
						tz= (parseInt(p[5], 10)*60);
						if(p[6]) tz+= parseInt(p[6], 10);
						if(p[4]== '+') tz*= -1;
						if(tz) day.setUTCMinutes(day.getUTCMinutes()+ tz);
				}
				return day;
		}
		return NaN;
};

UtilsManager.prototype.StopEvent = function(event) {
	if (utils.ObjectValid(event)) {
		if (utils.ObjectValid(event.stopPropagation)) {
			event.stopPropagation();
		}
		if (utils.ObjectValid(event.preventDefault)) {
			event.preventDefault();
		}
	}
}

UtilsManager.prototype.ObjectValid = function(theObject) {
	if (theObject == null) {
		return false;
	}
	if (typeof(theObject) == "undefined") {
		return false;
	}
	return true;
}
UtilsManager.prototype.ObjectsValid = function(theObjects) {
	for (var o = 0; o < theObjects.length; o++) {
		var object = theObjects[o];
		if (!utils.ObjectValid(object)) {
			return false;
		}
	}
	return true;
};

UtilsManager.prototype.RandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

UtilsManager.prototype.GetProperties = function(obj) {
	var props = "";
	for(var propt in obj){
		props += propt+" "+obj[propt]+"\n";
	}
	return props;
}

UtilsManager.prototype.ShowProperties = function(obj) {
	myConsole.Alert(utils.GetProperties(obj));
};

UtilsManager.prototype.LogProperties = function(obj) {
	myConsole.Log(utils.GetProperties(obj));
};

UtilsManager.prototype.BoolFromString = function(value) {
	if (!utils.ObjectValid(value)) {
		return false;
	}
	value = value.toLowerCase();
	if (value === "true" || value === "1") {
		return true;
	} else if (value === "false" || value === "0") {
		return false;
	}
	return false;
};

UtilsManager.prototype.PadNumericString = function(i) {
	return (i < 10) ? "0" + i : "" + i;
}	


UtilsManager.prototype.DelayCall = function(func, wait) {
	if (!utils.ObjectValid(func)) {
		myConsole.Alert("delayCall function not valid: "+func+" with wait: "+wait);
	}

	var args = Array.prototype.slice.call(arguments, 2);
	return setTimeout(function(){ return func.apply(null, args); }, wait);
};

UtilsManager.prototype.PixelIntFromString = function(pixelString) {
	var digit = parseInt(pixelString);
	if (utils.ObjectValid(pixelString)) {
		pixelString = ""+pixelString;
		var indexOfPx = pixelString.indexOf("px");
		if (indexOfPx >= 0) {
			digit = parseInt(pixelString.substring(0, indexOfPx));
		}
	}
	return parseInt(digit);
};

UtilsManager.prototype.CreateDOMElement = function(elType, elClass, elId) {
	if (!utils.ObjectValid(elType)) {
		elType = "div";
	}

	var newElement = $(document.createElement(elType));
	if (utils.ObjectValid(elClass)) {
		newElement.attr("class", elClass);
	}
	if (utils.ObjectValid(elId)) {
		newElement.attr("id", elId);
		newElement.id = elId;
	}
	return newElement;
}

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(obj, start) {
		 for (var i = (start || 0), j = this.length; i < j; i++) {
			 if (this[i] === obj) { return i; }
		 }
		 return -1;
	}
}

UtilsManager.prototype.GetElementHeight = function(element, fullCheck, dontCheckNaN) {
	var theElement = $(element);
	var height = null;
	
	if (fullCheck) {
		height = theElement.height() 
							+ parseInt(theElement.css('padding-top'), 10) 
							+ parseInt(theElement.css('padding-bottom'), 10)
							+ parseInt(theElement.css('margin-top'), 10) 
							+ parseInt(theElement.css('margin-bottom'), 10)
							+ parseInt(theElement.css('border-top-width'), 10)
							+ parseInt(theElement.css('border-bottom-width'), 10);
	} else {
		height = element.height();
	}
							
	if (utils.ElementIsInDOM(theElement) && (height === 0 || height+"" === "NaN") && !dontCheckNaN) {
		element.hide();
		$("body").append(element);
		height = utils.GetElementHeight(element, fullCheck, true);
		element.detach();
		element.show();
		
		return height;
	}							
							
	return height;
};


UtilsManager.prototype.GetElementWidth = function(element, fullCheck, dontCheckNaN) {
	var theElement = $(element);
	var width = null;
	
	if (fullCheck) {
		width = theElement.width() 
								+ parseInt(theElement.css('padding-left'), 10) 
								+ parseInt(theElement.css('padding-right'), 10)
								+ parseInt(theElement.css('margin-left'), 10) 
								+ parseInt(theElement.css('margin-right'), 10)
								+ parseInt(theElement.css('border-left-width'), 10)
								+ parseInt(theElement.css('border-right-width'), 10);
	} else {
		width = theElement.width();
	}

	if (utils.ElementIsInDOM(theElement) && (width === 0 || width+"" === "NaN") && !dontCheckNaN) {
		element.hide();
		$("body").append(element);
		width = utils.GetElementWidth(element, fullCheck, true);
		element.detach();
		element.show();
		
		return width;
	}							
	
	return width;
};

UtilsManager.prototype.ElementIsInDOM = function(element) {
	return !jQuery.contains(document.documentElement, $(element)[0]);
};

UtilsManager.prototype.FadeIn = function(element, speed, delay, oncomplete) {
	utils.Fade("in", element, speed, delay, oncomplete);
};

UtilsManager.prototype.FadeOut = function(element, speed, delay, oncomplete) {
	utils.Fade("out", element, speed, delay, oncomplete);
};

UtilsManager.prototype.Fade = function(type, element, speed, delay, oncomplete) {
	if (!utils.ObjectValid(element)) {
		return;
	}
	
	if (!utils.ObjectValid(speed)) {
		speed = "fast";
	}

	if ((type === "in" && element.css("opacity") === 1) || (type === "out" && element.css("opacity") === 0)) {
		return;
	}
	if (!utils.ObjectValid(delay)) {
		delay = 0;
	}
	
	var other = "out";
	if (type === "out") {
		other = "in";
	}
	
	var opacity = 0;
	if (type === "in") {
		opacity = 1;
	}

	utils.DelayCall(function(element, type, other, speed, opacity) {
		element.delay(delay).removeClass("fade"+other+"fast");
		element.delay(delay).removeClass("fade"+other+"slow");
		element.delay(delay).addClass("fade"+type+speed);
		element.delay(delay).css({"opacity":opacity, "visibility":opacity});
	}, delay, element, type, other, speed, opacity);
		
	var completeDelay = delay;
	if (speed === "fast") {
		completeDelay += 250;
	} else {
		completeDelay += 500;
	}

	utils.DelayCall(function(oncomplete, element) {
		element.removeClass("fade"+type+speed);
		if (utils.ObjectValid(oncomplete)) {
			oncomplete(element);
		}
	}, completeDelay, oncomplete, element);
};

UtilsManager.prototype.ShakeElement = function(element, times, amount, delay, axis) {
	if (!utils.ObjectValid(times)) {
		times = 2;
	}
	if (!utils.ObjectValid(amount)) {
		amount = 10;
	}
	if (!utils.ObjectValid(delay)) {
		delay = 50;
	}
	if (!utils.ObjectValid(axis)) {
		axis = "x";
	}

	element.css({
		"-webkit-transition": delay+"ms ease-in-out",
		"-0-transition": delay+"ms ease-in-out",
		"transition": delay+"ms ease-in-out"
	});

	var translateAxis = axis;
	
	if (translateAxis === "x") {
		element.css({
			"-webkit-transform": "translate3d(-"+amount+"px, 0, 0)",
			"-o-transform": "translate3d(-"+amount+"px, 0, 0)",
			"transform": "translate3d(-"+amount+"px, 0, 0)"
		});
	} else {
		element.css({
			"-webkit-transform": "translateY(-"+amount+"px)",
			"-o-transform": "translateY(-"+amount+"px)",
			"transform": "translateY(-"+amount+"px)"
		});
	}
	
	var flip = true;
	for (var t = 0; t < times-1; t++) {
		var position = amount;
		if (!flip) {
			position = -amount;
			flip = true;
		} else {
			flip = false;
		}
	
		utils.DelayCall(function(element, position) {
			if (translateAxis === "x") {
				element.css({
					"-webkit-transform": "translate3d("+position+"px, 0, 0)",
					"-o-transform": "translate3d("+position+"px, 0, 0)",
					"transform": "translate3d("+position+"px, 0, 0)"
				});
			} else {
				element.css({
					"-webkit-transform": "translateY("+position+"px)",
					"-o-transform": "translateY("+position+"px)",
					"transform": "translateY("+position+"px)"
				});
			}
		}, delay*t, element, position);
	}
	
	utils.DelayCall(function(element) {
		if (translateAxis === "x") {
			element.css({
				"-webkit-transform": "translate3d(0px, 0px, 0)",
				"-o-transform": "translate3d(0px, 0px, 0)",
				"transform": "translate3d(0px, 0px, 0)"
			});
		} else {
			element.css({
				"-webkit-transform": "translateY(0)",
				"-o-transform": "translateY(0)",
				"transform": "translateY(0)"
			});
		}
		
		utils.DelayCall(function(element) {
			element.css({
				"-webkit-transition": "",
				"-o-transition": "",
				"transition": "",
				"-webkit-transform": "",
				"-o-transform": "",
				"transform": ""
			});
		}, 10, element);
	}, delay*times, element);
};

UtilsManager.prototype.PulseElement = function(element, amount, delay, initial, axis, origin, onComplete) {
	if (!utils.ObjectValid(amount)) {
		amount = 1.1;
	}
	if (!utils.ObjectValid(delay)) {
		delay = 50;
	}
	if (!utils.ObjectValid(axis)) {
		axis = "both";
	}
	
	if (utils.ObjectValid(initial)) {
		element.css({
			"-webkit-transition": "0",
			"-o-transition": "0",
			"transition": "0",
		});

		if (axis === "x") {
			element.css({
				"-webkit-transform": "scaleX("+initial+")",
				"-o-transform": "scaleX("+initial+")",
				"transform": "scaleX("+initial+")"
			});
		} else if (axis === "y") {
			element.css({
				"-webkit-transform": "scaleY("+initial+")",
				"-o-transform": "scaleY("+initial+")",
				"transform": "scaleY("+initial+")"
			});
		} else {
			element.css({
				"-webkit-transform": "scale("+initial+", "+initial+")",
				"-o-transform": "scale("+initial+", "+initial+")",
				"transform": "scale("+initial+", "+initial+")"
			});
		}	

		utils.DelayCall(function(element, amount, delay, axis, origin, onComplete) {
			element.css({
				"-webkit-transition": "-webkit-transform "+delay+"ms ease-in-out",
				"-o-transition": "-o-transform "+delay+"ms ease-in-out",
				"transition": "transform "+delay+"ms ease-in-out",
			});

			utils.DelayCall(function(element, amount, delay, axis, origin, onComplete) {
				utils.PulseElement(element, amount, delay, null, axis, origin, onComplete);
			}, delay, element, amount, delay, axis, origin, onComplete);
		}, delay, element, amount, delay, axis, origin, onComplete);
		return;		
	}

	element.css({
		"-webkit-transition": "-webkit-transform "+delay+"ms ease-in-out",
		"-o-transition": "-o-transform "+delay+"ms ease-in-out",
		"transition": "transform "+delay+"ms ease-in-out",
	});
	if (axis === "x") {
		element.css({
			"-webkit-transform": "scaleX("+amount+")",
			"-o-transform": "scaleX("+amount+")",
			"transform": "scaleX("+amount+")"
		});
	} else if (axis === "y") {
		element.css({
			"-webkit-transform": "scaleY("+amount+")",
			"-o-transform": "scaleY("+amount+")",
			"transform": "scaleY("+amount+")"
		});
	} else {
		element.css({
			"-webkit-transform": "scale("+amount+", "+amount+")",
			"-o-transform": "scale("+amount+", "+amount+")",
			"transform": "scale("+amount+", "+amount+")"
		});
	}	
	
	if (utils.ObjectValid(origin)) {
		element.css({
			"-webkit-transform-origin": origin,
			"-o-transform-origin": origin,
			"transform-origin": origin
		});
	}
	
	utils.DelayCall(function(element, completeFunc) {
		element.css({
			"-webkit-transition": "",
			"-o-transition": "",
			"transition": "",
			"-webkit-transform-origin": "",
			"-o-transform-origin": "",
			"transform-origin": "",
			"-webkit-transform": "",
			"-o-transform": "",
			"transform": ""
		});
		
		if (utils.ObjectValid(completeFunc)) {
			completeFunc();
		}		
	}, delay, element, onComplete);
};

UtilsManager.prototype.Hide = function(element) {
	element.removeClass("fadeinfast");
	element.removeClass("fadeinslow");
	element.removeClass("fadeoutfast");
	element.removeClass("fadeoutslow");
	element.css({"opacity":0});
};

UtilsManager.prototype.SlideTo = function(element, to) {
	var from = $(window).scrollTop();
  var dy = to-from;
  element = $(element);

	element.css({"margin-top": dy+"px"});
	$(window).scrollTop(to);

	element.css({
		"-webkit-transition":"margin-top 1s ease",
		"-o-transition":"margin-top 1s ease",
		"transition":"margin-top 1s ease"
	});
	element.css({"margin-top": 0});

	element.on("webkitTransitionEnd transitionend msTransitionEnd oTransitionEnd", function(){
	});
};

UtilsManager.prototype.AnimateClicked = function(element) {
	var sizeMultiplier = 1.25;

	element.css({
		"-webkit-transition":"none",
		"-o-transition":"none",
		"transition":"none",
		scale: [1.25, 1.25]
	});
	
	utils.DelayCall(function(element) {
		element.css({
			"-webkit-transition":"none",
			"-o-transition":"none",
			"transition":"none",
			scale: [1.0, 1.0]
		});
	}, 250, element);
};

UtilsManager.prototype.Truncate = function(string, length) {
   if (string.length > length)
      return string.substring(0,length)+'...';
   else
      return string;
};

UtilsManager.prototype.ElementHasClass = function(element, className, checkPartial) {
	var classes = $(element).attr("class").split(" ");
	for (var c = 0; c < classes.length; c++) {
		var elClass = classes[c];
		if (elClass === className) {
			return true;
		} else if (utils.ObjectValid(checkPartial) && checkPartial && elClass.indexOf(className) >= 0) {
			return true;
		}
	}
	return false;
};

UtilsManager.prototype.ComponentToHex = function(c) {
    var hex = c.toString(16);
    return hex.length == 1 ? "0" + hex : hex;
};

UtilsManager.prototype.RGBToHex = function(r, g, b) {
    return "#" + componentToHex(r) + componentToHex(g) + componentToHex(b);
};

UtilsManager.prototype.HexToRGB = function(hex) {
		if (hex.substring(0, 1) !== "#") {
			return hex;
		}

    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    if (!utils.ObjectValid(result)) {
    	return hex;
    }
    
    result = "rgb("+parseInt(result[1], 16)+", "+parseInt(result[2], 16)+", "+parseInt(result[3], 16)+")";
    return result;
};

UtilsManager.prototype.AddClickEvent = function(element, onClick, reAttach, attachDelay) {
	var doReAttach = reAttach;
	if (!utils.ObjectValid(doReAttach)) {
		doReAttach = false;
	}
	
	var doDelay = null;
	if (doReAttach) {
		if (utils.ObjectValid(attachDelay)) {
			doDelay = attachDelay;
		}
		if (!utils.ObjectValid(doDelay)) {
			doDelay = 100;
		}
	}

	var clickFunc = onClick;

	element.unbind("click");
	element.click(function() {
		var thisObject = $(this);
		thisObject.unbind("click");
		
		clickFunc();
		
		if (doReAttach) {
			utils.DelayCall(function(clickObject, onClickFunc) {
				utils.AddClickEvent(clickObject, onClickFunc);
			}, doDelay, thisObject, clickFunc);
		}
	});
};

UtilsManager.prototype.DaysBetweenDates = function(first, second) {
	var firstSeconds = first.getTime()/1000;
	var secondSeconds = second.getTime()/1000;
	
	var diff = secondSeconds-firstSeconds;
	var numDays = Math.floor(diff % 86400);
	return numDays;	
}

UtilsManager.prototype.DateToDDMMYYYY = function(date) {
	var dd = date.getDate();
	var mm = date.getMonth()+1; //January is 0!

	var yyyy = date.getFullYear();
	if (dd<10) {
		dd='0'+dd;
	}
	if (mm<10) {
		mm='0'+mm;
	}
	
	var newDate = dd+'/'+mm+'/'+yyyy;
	return newDate;
};

UtilsManager.prototype.DateTimeToDDMMYYYYHHMMSS = function(date) {
	var newString = ("00" + date.getDate()).slice(-2) + "/" +
									("00" + (date.getMonth() + 1)).slice(-2) + "/" + 
							    date.getFullYear() + " " + 
							    ("00" + date.getHours()).slice(-2) + ":" + 
							    ("00" + date.getMinutes()).slice(-2) + ":" + 
							    ("00" + date.getSeconds()).slice(-2);
	return newString;
};

UtilsManager.prototype.SecondsSinceEpoch = function() {
	var d = new Date();
	var seconds = d.getTime() / 1000;
	return seconds;
};

UtilsManager.prototype.CapitaliseFirstLetter = function(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
};

UtilsManager.prototype.UrlExists = function(url) {
	var http = new XMLHttpRequest();
	http.open('HEAD', url, false);
	http.send();
	
	return http.status!=404;
}

UtilsManager.prototype.FileNameFromUrl = function(url) {
	var splitUrl = url.split("/");
	if (!utils.ObjectValid(splitUrl) || splitUrl.length <= 0) {
		return url;
	}
	return splitUrl[splitUrl.length-1];
};

UtilsManager.prototype.IsLeapYear = function(year) {
  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
};

UtilsManager.prototype.ValidateEmail = function(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
};

UtilsManager.prototype.DayOrdinal = function(number) {
  var d = number % 10;
  return (~~ (number % 100 / 10) === 1) ? 'th' :
         (d === 1) ? 'st' :
         (d === 2) ? 'nd' :
         (d === 3) ? 'rd' : 'th';
};

UtilsManager.prototype.GetDaysInMonthFromDate = function(date) {
	return new Date(date.getFullYear(), date.getMonth(), 0).getDate();
};

UtilsManager.prototype.TimeOfDay = function(date) {
	var hour = date.getHours();
	
	var timeOfDay = "Night";
	if (hour > 5 && hour < 12) {
		timeOfDay = "Morning";
	} else if (hour < 17) {
		timeOfDay = "Afternoon";
	} else if (hour < 22) {
		timeOfDay = "Evening";
	}
	return timeOfDay;
};

UtilsManager.prototype.GenerateUUID = function() {
	var d = new Date().getTime();
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
			var r = (d + Math.random()*16)%16 | 0;
			d = Math.floor(d/16);
			return (c=='x' ? r : (r&0x7|0x8)).toString(16);
	});
	return uuid;
};

UtilsManager.prototype.MakeSafeString = function(str) {
	if (!utils.ObjectValid(str) || str.length <= 0) {
		return str;
	}
	var i = str.length;
  var aRet = [];

  while (i--) {
    var iC = str[i].charCodeAt();
    if (iC < 65 || iC > 127 || (iC>90 && iC<97)) {
      aRet[i] = '&#'+iC+';';
    } else {
      aRet[i] = str[i];
    }
	}
  str = aRet.join('');
	str = str.replace(/&#32;/g, " ");
	str = str.replace(/&#48;/g, "0").replace(/&#49;/g, "1").replace(/&#50;/g, "2").replace(/&#51;/g, "3").replace(/&#52;/g, "4").replace(/&#53;/g, "5").replace(/&#54;/g, "6").replace(/&#55;/g, "7").replace(/&#56;/g, "8").replace(/&#57;/g, "9");
	
	return str;
};

UtilsManager.prototype.RevertSafeString = function(str) {
	return utils.DecodeEntities(str);
};

UtilsManager.prototype.DecodeEntities = function(str) {
	return $('<div/>').html(str).text();
};

UtilsManager.prototype.URLCanEmbed = function(url, bypass, onComplete) {
	var completeFunc = onComplete;
	var backupUrl = url;
	var theUrl = url;
	
	if (!bypass) {
		theUrl = "http://sgiserver.co.uk/origindetect.php?url="+encodeURIComponent(url);

		$.ajax({ 
			type: "GET",
			cache: false,
			url: theUrl,
			success: function (data) {
				data = JSON.parse(data);
				
				if (data.error || data.httpcode !== 200) {
					if (utils.ObjectValid(completeFunc)) {
						completeFunc(backupUrl, false);
					}
				} else {
					if (utils.ObjectValid(completeFunc)) {
						completeFunc(backupUrl, true);
					}
				}
			},
			error: function (ajaxContext) {
				utils.URLCanEmbed(backupUrl, true, completeFunc);
			}
		});
	} else {
		$.ajax({ 
			cache: false,
			url: url,
			success: function (data) {
				if (utils.ObjectValid(completeFunc)) {
					completeFunc(backupUrl, true);
				}
			},
			error: function (ajaxContext) {
				if (utils.ObjectValid(completeFunc)) {
					var success = true;
					if (ajaxContext.status !== 200) {
						success = false;
					}
					completeFunc(backupUrl, success);
				}
			}
		});
	}
};

UtilsManager.prototype.AppendArrayToArray = function(destination, source) {
	if (!utils.ObjectValid(destination)) {
		destination = new Array();
	}
	
	for (var i = 0; i < source.length; i++) {
		var item = source[i];
		destination.push(item);
	}
	return destination;
};

UtilsManager.prototype.SplitStringIntoArrayWithDelimeter = function(string, delim) {
	var objects = string.split(delim);
	for (var p = 0; p < objects.length; p++) {
		var newObject = objects[p];
		var spaceIndex = newObject.indexOf(" ");
		if (newObject.length > 1 && spaceIndex === 0) {
			newObject = newObject.substring(spaceIndex+1, newObject.length);
			objects[p] = newObject;
		}
	}
	return objects;
};

UtilsManager.prototype.RemoveLeadingSpacesFromString = function(string) {
	while (true) {
		var checkChar = string.charAt(0);
		if (checkChar === " " || checkChar === "	") {
			string = string.substring(1, string.length);
		} else {
			break;
		}
	}
	return string;
};

UtilsManager.prototype.RemoveTrailingSpacesFromString = function(string) {
	while (true) {
		var checkChar = string.charAt(string.length-1);
		if (checkChar === " " || checkChar === "	") {
			string = string.substring(0, string.length-1);
		} else {
			break;
		}
	}
	return string;
};

UtilsManager.prototype.RemoveStringFromEndOfString = function(string, match, recursive, andSpaces) {
	if (string.length < match.length) {
		return string;
	}
	
	if (andSpaces) {
		string = utils.RemoveTrailingSpacesFromString(string);
	}
	
	var checkString = string.substring(string.length-match.length, string.length);
	if (checkString === match) {
		string = string.substring(0, string.length-match.length);
		if (!recursive) {
			return string;
		}
		return utils.RemoveStringFromEndOfString(string, match, recursive, andSpaces);
	}
	return string;
};

UtilsManager.prototype.GetAbsolutePath = function() {
	var loc = window.location;
	var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
	return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
}

UtilsManager.prototype.IsArray = function(obj) {
	if (Object.prototype.toString.call(obj) === '[object Array]' ) {
		return true;
	}
	return false;
};
UtilsManager.prototype.IsString = function(obj) {
	if (typeof obj === "string") {
  	return true;
	}
	return false;
};

UtilsManager.prototype.PreloadImage = function(image) {
	var file = new Image();
	file.src = image;
	return file;
};

UtilsManager.prototype.SetElementParagraph = function(element, content) {
	if (!utils.ObjectValid(element) || element.length <= 0) {
		myConsole.Log("SetElementParagraph element invalid...");
		utils.ShowProperties(element);
		return;
	}
	
	element.html("<p>"+content+"</p>");
};

UtilsManager.prototype.GetLocationParameters = function() {
	var url = window.location.href;
	var index = url.indexOf("?");
	if (index <= -1) {
		return "";
	}
	return url.substring(index+1);
};

var utils = new UtilsManager();

/*
jQuery.fn.doubleHide = function(){
    return $(this).hide().css('opacity', '0');
};

jQuery.fn.slideFade = function(slide, fade){
	return $(this).slideUp(slide, function () {
    $(this).fadeTo(fade, 100);
  });
};

jQuery.fn.countTo = function(options) {
	// merge the default plugin settings with the custom options
	options = $.extend({}, $.fn.countTo.defaults, options || {});

	// how many times to update the value, and how much to increment the value on each update
	var loops = Math.ceil(options.speed / options.refreshInterval),
			increment = (options.to - options.from) / loops;

	return $(this).each(function() {
			var _this = this,
					loopCount = 0,
					value = options.from,
					interval = setInterval(updateTimer, options.refreshInterval);

			function updateTimer() {
					value += increment;
					loopCount++;
					$(_this).html(value.toFixed(options.decimals));

					if (typeof(options.onUpdate) == 'function') {
							options.onUpdate.call(_this, value);
					}

					if (loopCount >= loops) {
							clearInterval(interval);
							value = options.to;

							if (typeof(options.onComplete) == 'function') {
									options.onComplete.call(_this, value);
							}
					}
			}
	});
};

jQuery.fn.countTo.defaults = {
	from: 0,  // the number the element should start at
	to: 100,  // the number the element should end at
	speed: 1000,  // how long it should take to count between the target numbers
	refreshInterval: 100,  // how often the element should be updated
	decimals: 0,  // the number of decimal places to show
	onUpdate: null,  // callback method for every time the element is updated,
	onComplete: null,  // callback method for when the element finishes updating
};

jQuery.fn.scrollStopped = function(callback) {          
    $(this).scroll(function(){
        var self = this, $this = $(self);
        if ($this.data('scrollTimeout')) {
          clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback,250,self));
    });
}; */