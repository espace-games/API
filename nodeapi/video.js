Europeana.prototype.URLHasValidVideoExtension = function(fileUrl, allowEmpty) {
	if (!utils.ObjectValid(fileUrl)) {
		return false;
	}

	fileUrl = fileUrl.toLowerCase();
	if (allowEmpty) {
		var slash = fileUrl.lastIndexOf("/");
		if (slash > -1) {
			var dot = fileUrl.lastIndexOf(".");
			console.log("slash "+slash+" dot "+dot);
			if (dot < slash) {
				return true;
			}
		}
	}

	for (var e = 0; e < europeana.EXTENSIONS.VIDEO.length; e++) {
		var extension = europeana.EXTENSIONS.VIDEO[e];
		
		if (fileUrl.indexOf("."+extension) > -1) {
//			console.log("**** URL HAS VALID EXTENSION "+fileUrl);
			return true;
		}
	}
	
	return false;
};

Europeana.prototype.GetVideo = function(fileUrl, domain, allowRecursion, allowEmptyExtension, onComplete) {
	myConsole.Log("GetVideo "+fileUrl);
	
	if (!utils.ObjectValid(fileUrl) || fileUrl.indexOf("#") > -1) {
		myConsole.Log("GetVideo url error: "+fileUrl);
		if (utils.ObjectValid(onComplete)) {
			onComplete(null);
		}
		return;
	}

	var theUrl = fileUrl;
	var completeFunc = onComplete;
	var canRecurse = allowRecursion;
	var theDomain = domain;
	var emptyExtension = allowEmptyExtension;

	if (europeana.URLHasValidVideoExtension(theUrl, emptyExtension)) {
		myConsole.Log("Video URL has a valid extension! "+theUrl);
/*
		europeana.CheckURLExists(theUrl, function(exists, error, response, body) {
			if (exists) {
*/			
				var video = new Object;
				video.url = theUrl;
	
				if (utils.ObjectValid(completeFunc)) {
					completeFunc(video);
				}
/*
			} else {
				myConsole.Log("Video page does not exist! "+theUrl);

				if (utils.ObjectValid(completeFunc)) {
					completeFunc(null);
				}
			}
		});
*/			
	} else {
		myConsole.Log("Video does not have a valid extension: "+theUrl);

		if (utils.ObjectValid(completeFunc)) {
			completeFunc(null);
		}
	}
};

Europeana.prototype.RequestAndGetVideoInPage = function(pageUrl, domain, allowRecursion, onComplete) {
	var completeFunc = onComplete;
	var theDomain = domain;

	europeana.VisitedURL(pageUrl);

	var pageRequest = request({
		uri: pageUrl,
	}, function(error, response, body) {
		myConsole.Log("Getting videos in page "+pageUrl);
		body = cheerio.load(body);
		
		europeana.GetVideoInPage(body, theDomain, allowRecursion, function(video) {
			myConsole.Log("out of GetVideoInPage in RequestAndGetVideoInPage, video: "+video);

			if (utils.ObjectValid(completeFunc)) {
				myConsole.Log("RequestAndGetVideoInPage completefunc "+video);
				completeFunc(video);
			}
		});
	});
	
	return pageRequest;
};

Europeana.prototype.GetVideoInPage = function(body, domain, allowRecursion, onComplete) {
	var $ = body;
	var theDomain = domain;
	var completeFunc = onComplete;
	var canRecurse = allowRecursion;
	
	var htmlString = $("body").html();
	
	var videos = new Array();
	var videoElements = $("param");
	if (videoElements.length <= 0) {
		var index = 0;
	
		while (true) {
			var nextIndex = htmlString.indexOf("<param ", index);
			
			var end = false;
			if (nextIndex > index) {
				var checkClose = " />";
				var closeIndex = htmlString.indexOf(checkClose, nextIndex);
				
				if (closeIndex <= -1) {
					end = true;
				} else {
					var endIndex = closeIndex+checkClose.length;
					var tag = htmlString.substring(nextIndex, endIndex);
					videoElements.push(tag);
					
					index = endIndex;
				}
			} else {
				end = true;
			}
			
			if (end) {
				break;
			}	
		}
	}
	
	for (var s = 0; s < videoElements.length; s++) {
		var element = $(videoElements[s]);
		var name = element.attr("name");
		myConsole.Log("Got param element with name: "+name);
		if (name.toLowerCase() === "flashvars") {
			element.type = "param";

			myConsole.Log("Got potential video with param element...");
			videos.push(element);
		}
	}
	videoElements = $("body").find("embed");
	for (var s = 0; s < videoElements.length; s++) {
		var element = $(videoElements[s]);
		var flashvars = element.attr("flashvars");
		console.log("Got embed... "+flashvars);
		utils.LogProperties(element);

		if (utils.ObjectValid(flashvars) && flashvars.length > 0) {
			element.type = "embed";

			myConsole.Log("Got potential video with embed element...");
			videos.push(element);
		}
	}
	videoElements = $("a");
	for (var s = 0; s < videoElements.length; s++) {
		var element = $(videoElements[s]);
		element.type = "a";
		
		myConsole.Log("Got a tag: "+element.attr("href"));
		videos.push(element);
	}

	if (videos.length <= 0) {
		if (utils.ObjectValid(completeFunc)) {
			completeFunc(null);
		}
	} else {
		var videoUrls = new Array();
	
		for (var s = 0; s < videos.length; s++) {
			var element = videos[s];

			var src = null;		
			if (element.type === "param") {
				src = element.attr("value");
				
				console.log("Checking param tag with value: "+src);

				var check = "config=";
				var index = src.indexOf(check);	
				if (index > -1) {
					var dataString = src.substring(check.length);
					myConsole.Log("Checking video param dataString: "+dataString);

					var data = JSON.parse(dataString);
					if (utils.ObjectValid(data)) {
						if (utils.ObjectValid(data.playlist)) {
							for (var v = 0; v < data.playlist.length; v++) {
								var video = data.playlist[v];
								if (!utils.ObjectValid(video) || !utils.ObjectValid(video.url)) {
									continue;
								}
								if (!europeana.URLHasValidVideoExtension(video.url, false)) {
									continue;
								}

								var videoUrl = video.url;						
								var index = videoUrls.indexOf(videoUrl);
								if (index <= -1) {
									videoUrls.push(videoUrl);
								}
							}
						}
					}
				}

				check = "videoUrl=";
				index = src.indexOf(check);	
				if (index > -1) {
					var dataString = src.substring(index+check.length);
					console.log("data string: "+dataString);
					index = dataString.indexOf("&");
					if (index > -1) {
						dataString = dataString.substring(0, index);
					}
					myConsole.Log("Checking video embed dataString: "+dataString);
					var decoded = decodeURIComponent(url.parse(dataString, true).path.replace(/\++/g, ' '));
					myConsole.Log("Decoded: "+decoded);
					
					if (europeana.URLHasValidVideoExtension(decoded, false)) {
						videoUrls.push(decoded);
					}
				}
			} else if (element.type === "embed") {
				src = element.attr("flashvars");

				var check = "videoUrl=";
				var index = src.indexOf(check);	
				if (index > -1) {
					var dataString = src.substring(check.length);
					index = dataString.indexOf("&");
					if (index > -1) {
						dataString = dataString.substring(0, index);
					}
					myConsole.Log("Checking video embed dataString: "+dataString);
					
					if (europeana.URLHasValidVideoExtension(dataString, false)) {
						videoUrls.push(dataString);
					}
				}
			} else if (element.type === "a") {
				var src = element.attr("href");
				var index = videoUrls.indexOf(src);
				if (index <= -1 && europeana.URLHasValidVideoExtension(src, false)) {
					videoUrls.push(src);
				}
			}
		}
		
		var theVideoUrls = videoUrls;
		var theVideos = videos;
		var videoData = new Array();
		var videoIndex = 0;
		
		for (var v = 0; v < videoUrls.length; v++) {
			var src = videoUrls[v];
		
			var fullAddress = europeana.GetAbsoluteURL(theDomain, src);

			europeana.GetVideo(fullAddress, theDomain, canRecurse, false, function(video) {
				europeana.VisitedURL(fullAddress);
			
				var showUrl = null;
				if (utils.ObjectValid(video)) {
					showUrl = video.url;
					videoData.push(video);
				}
		
				videoIndex++;
				
				console.log("videoIndex "+videoIndex+" videoUrls "+theVideoUrls.length);

				if (videoIndex >= theVideoUrls.length) {
					if (utils.ObjectValid(completeFunc)) {
						var theVideo = europeana.GetBestVideoFromArray(videoData);
						completeFunc(theVideo);
					}
				}
			});
		}
	}
};

Europeana.prototype.GetBestVideoFromArray = function(videoArray) {
	myConsole.Log("GetBestVideoFromArray "+videoArray.length);

	var best = null;
	if (videoArray.length > 0) {
		for (var e = 0; e < europeana.EXTENSIONS.VIDEO.length; e++) {
			var extension = "."+europeana.EXTENSIONS.VIDEO[e].toLowerCase();
			var foundIt = false;
			for (var s = 0; s < videoArray.length; s++) {
				best = videoArray[0];
				if (!utils.ObjectsValid([best, best.url])) {
					best = null;
					continue;
				}
				var theUrl = best.url.toLowerCase();
//				myConsole.Log("Testing url: "+theUrl+" against extension: "+extension);
				if (theUrl.indexOf(extension) > -1) {
//					myConsole.Log("Match!");
					foundIt = true;
					break;
				} else {
//					myConsole.Log("No match!");
					best = null;
				}
			}
			
			if (foundIt) {
				break;
			}
		}
	}
	
	return best;
};

Europeana.prototype.RespondWithVideoData = function(response, video) {
	if (!utils.ObjectsValid([response, video])) {
		return false;
	}
		
	europeana.SendResponse(response, {status: europeana.ERRORS.NONE, location: video.url});
	return true;
};

Europeana.prototype.FinishRecordVideo = function(response, video) {
	if (!utils.ObjectValid(video)) {
		europeana.SendResponse(response, {status: europeana.ERRORS.ERROR_NONE_FOUND, contentType: europeana.MIME.TEXT, data: "Couldn't get a video!"});
		return;
	}

	if (!europeana.RespondWithVideoData(response, video)) {
		europeana.SendResponse(response, {status: europeana.ERRORS.RETRIEVING, contentType: europeana.MIME.TEXT, data: "Tried to get the video in the page but something went wrong!"});
	}
};

Europeana.prototype.GetRecordVideo = function(currentDomain, visited, response, recordId) {
	var theRes = response;
	var theId = recordId;

	var theDomain = currentDomain;

	var queryString = europeana.APIURL+"/record"+recordId+".json?wskey="+europeana.KEY+"&profile="+europeana.PROFILES.RECORD;
	europeana.Query(queryString, function(response, data) {
		var resultObject = null;
		try {
			resultObject = JSON.parse(data);
		} catch (error) {
			europeana.FinishRecordVideo(theRes, null);
			return;		
		}

		var item = europeana.GetObjectData("minimal", theId, resultObject);
		if (utils.ObjectValid(item.unretrievable)) {
			europeana.CantGetAsset(theRes, item.unretrievable);
			return;
		}

		if (item.type.toLowerCase() !== "video") {
			europeana.FinishRecordVideo(theRes, null);
			return;		
		}
		
		europeana.GetVideo(item.url, null, false, true, function(video) {
			europeana.VisitedURL(item.url);

			if (utils.ObjectValid(video)) {
				europeana.FinishRecordVideo(theRes, video);
			} else {
				europeana.GetVideo(item.object, null, false, true, function(video) {
					europeana.VisitedURL(item.object);

					if (utils.ObjectValid(video)) {
						europeana.FinishRecordVideo(theRes, video);
					} else {
						if (!utils.ObjectValid(item.page)) {
							europeana.FinishRecordVideo(theRes, null);
						} else {
							if (!utils.ObjectValid(item.page)) {
								europeana.FinishRecordVideo(theRes, siteImages);
							} else {
								myConsole.Log("Scraping page "+item.page);

								europeana.VisitedURL(item.page);
								
								var videoRequest = request({
									uri: item.page,
								}, function(error, response, body) {
									body = cheerio.load(body);
									var $ = body;

									var domain = europeana.GetBaseURLOfPage(item.page, body, response);
									theDomain = europeana.SetCurrentDomain(domain);
				
									myConsole.Log("Domain: "+domain);
		
									myConsole.Log("Getting videos in page "+item.page);
									europeana.GetVideoInPage(body, domain, true, function(video) {
										myConsole.Log("out of GetVideoInPage in GetRecordVideo, video: "+video);
							
										if (utils.ObjectValid(video)) {
											myConsole.Log("GetRecordVideo FinishRecordVideo "+video);
											europeana.FinishRecordVideo(theRes, video);
										} else {
											var frames = $("frame");
									
											if (frames.length <= 0) {
												europeana.FinishRecordVideo(theRes, null);
											} else {
												var framesVideos = new Array();
		
												var frameIndex = 0;
		
												frames.each(function() {
													var frame = $(this);
			
													var src = frame.attr("src");
			
													var address = null;
													if (src.indexOf("http") === 0) {
														address = src;
													} else if (utils.ObjectValid(domain)) {
														address = domain+"/"+src;
													}
			
													var frameRequest = europeana.RequestAndGetVideoInPage(address, domain, true, function(video) {
														if (utils.ObjectValid(video)) {
															framesVideos.push(video);
														}
									
														frameIndex++;
								
														if (frameIndex >= frames.length) {
															var theVideo = europeana.GetBestVideoFromArray(framesVideos);
															europeana.FinishRecordVideo(theRes, theVideo);
														}
													});
												});
											}
										}
									});										
								});
							}
						}
					}
				});
			}
		});
	});
};
