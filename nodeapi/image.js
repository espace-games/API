Europeana.prototype.URLHasValidImageExtension = function(fileUrl) {
	if (!utils.ObjectValid(fileUrl)) {
		return false;
	}

	for (var e = 0; e < europeana.EXTENSIONS.IMAGE.length; e++) {
		var extension = europeana.EXTENSIONS.IMAGE[e];
		var check = fileUrl.toLowerCase();
		
		if (check.indexOf("."+extension) > -1) {
//			console.log("**** URL HAS VALID EXTENSION "+fileUrl);
			return true;
		}
	}
	
	return false;
};

Europeana.prototype.FinishImage = function(theUrl, data, contentType, onComplete) {
//	myConsole.Log("FinishImage "+theUrl);

	var dimensions = null;
	var image = null;

	try {
		dimensions = sizeOf(data);
	} catch (error) {
		myConsole.Log("FinishImage with url: "+theUrl+" dimensions error: "+error.message);
		
		dimensions = null;
	}

	image = new Object;
	image.data = data;
	image.url = theUrl;
	
	
	if (utils.ObjectValid(dimensions)) {
		image.width = dimensions.width;
		image.height = dimensions.height;
	}
	
	if (utils.ObjectValid(contentType)) {
		image.contentType = contentType;
	}
	
	if (utils.ObjectValid(onComplete)) {
		onComplete(image);
	}
};

Europeana.prototype.DownloadImage = function(imageUrl, content, onComplete) {
//	myConsole.Log("DownloadImage "+imageUrl);

	var check = "maxwidth=";
	var index = imageUrl.indexOf(check);
	if (index > -1) {
		var endIndex = imageUrl.indexOf("&", index+check.length);
		
		var newUrl = imageUrl.substring(0, index+check.length)+"5000";
		if (endIndex > -1) {
			newUrl += imageUrl.substring(endIndex);
		}
		imageUrl = newUrl;
	}

	var theUrl = imageUrl;
	var completeFunc = onComplete;

	var options = null;
	
	try {
		options = url.parse(imageUrl);
	} catch (error) {
		options = null;
	}
	
	if (!utils.ObjectValid(options)) {
		myConsole.Log("DownloadImage options error: "+imageUrl);
		if (utils.ObjectValid(onComplete)) {
			onComplete(null);
		}
		return;
	}
	

	var contentType = content;
	var getProtocol = http;
	
	if (imageUrl.indexOf("https") === 0) {
		getProtocol = https;
	}

	try {
		getProtocol.get(options, function (response) {
			var newType = response.headers["content-type"];
			if (utils.ObjectValid(newType)) {
				var index = ";";
				if (index > -1) {
					newType = newType.substring(0, index);
				}
			
//				myConsole.Log("Got new content type: "+newType);
				contentType = newType;
			}
			
			var chunks = [];
			response.on('data', function (chunk) {
				chunks.push(chunk);
			}).on('end', function() {
				var buffer = Buffer.concat(chunks);
//				myConsole.Log("DownloadImage end for "+theUrl);

				europeana.FinishImage(theUrl, buffer, contentType, completeFunc);
			});
		});
	} catch (error) {
		myConsole.Log("DownloadImage get error: "+error.message);
		if (utils.ObjectValid(onComplete)) {
			onComplete(null);
		}
	}
};

Europeana.prototype.GetImage = function(visited, imageUrl, forceCheckRedirect, onComplete) {
//	myConsole.Log("GetImage "+imageUrl);
	
	if (!utils.ObjectValid(imageUrl)) {
//		myConsole.Log("GetImage url error: "+imageUrl);
		if (utils.ObjectValid(onComplete)) {
			onComplete(null);
		}
		return;
	}
	
	if (!forceCheckRedirect && europeana.HaveVisitedURL(visited, imageUrl)) {
//		myConsole.Log("GetImage already visited: "+imageUrl);
		if (utils.ObjectValid(onComplete)) {
			onComplete(null);
		}
		return;
	}

	europeana.VisitedURL(visited, imageUrl);
	
	var protocol = "http";
	if (imageUrl.indexOf("https") === 0) {
		protocol += "s";
	}

	if (!forceCheckRedirect && europeana.URLHasValidImageExtension(imageUrl)) {
		var visitedList = visited;
		var theUrl = imageUrl;
		var completeFunc = onComplete;

		europeana.DownloadImage(imageUrl, null, function(image) {
			if (utils.ObjectValid(image) && utils.ObjectValid(image.contentType) && image.contentType.toLowerCase().indexOf("image") > -1) {
				if (utils.ObjectValid(completeFunc)) {
					completeFunc(image);
				}
			} else {
				europeana.GetImage(visitedList, theUrl, true, completeFunc);
			}
		});
	} else {
		var completeFunc = onComplete;
		var theUrl = imageUrl;
	
		var pageRequest = request({
			uri: imageUrl,
		}, function(error, response, body) {
			var contentType = "";
		
			if (utils.ObjectValid(response) && utils.ObjectValid(response.headers)) {
				contentType = response.headers["content-type"];
			}
//			console.log("Got content type "+contentType+" from url "+theUrl);
			if (contentType.toLowerCase().indexOf("image") > -1) {
				var uri = protocol+"://"+response.req._headers.host+response.req.path;
				europeana.DownloadImage(uri, contentType, completeFunc);
			} else {
//				console.log("No image in content type: "+contentType+" from url: "+imageUrl);
				if (utils.ObjectValid(completeFunc)) {
					completeFunc(null);
				}
			}
		});			
	}
};

Europeana.prototype.ProcessImageAndRespond = function(response, image, imageOptions, onComplete) {
//	myConsole.Log("ProcessImageAndRespond "+image);

	var completeFunc = onComplete;

	if (!utils.ObjectsValid([response, image])) {
		if (utils.ObjectValid(completeFunc)) {
			completeFunc(false);
		}
		return;
	}
	
	var filename = image.url.substring(image.url.lastIndexOf("/"));
	var theRes = response;
	var theImage = image;
	
	image.data = gm(image.data, filename);
	
	if (utils.ObjectValid(imageOptions.crop) && imageOptions.crop > 0) {
		var crop = new Object;
		crop.amount = imageOptions.crop/100;
		crop.horizontal = image.width*crop.amount;
		crop.vertical = image.height*crop.amount;

//		myConsole.Log("**** CROP BEFORE w: "+image.width+" h: "+image.height+" a: "+crop.amount+" x: "+crop.horizontal+" y: "+crop.vertical);
		
		image.width = parseInt(image.width-(crop.horizontal*2));
		image.height = parseInt(image.height-(crop.horizontal*2));
		
//		myConsole.Log("**** CROPPING w: "+image.width+" h: "+image.height+" x: "+crop.horizontal+" y: "+crop.vertical);
		
		image.data = image.data.crop(image.width, image.height, crop.horizontal, crop.vertical);
	}

	if (!utils.ObjectValid(imageOptions.aspect)) {
		imageOptions.aspect = false;
	}

	var aspect = image.width/image.height;
	if (utils.ObjectValid(imageOptions.width) && !utils.ObjectValid(imageOptions.height)) {
		if (imageOptions.aspect) {
			imageOptions.height = imageOptions.width/aspect;
		} else {
			imageOptions.height = image.height;
		}
	} else if (utils.ObjectValid(imageOptions.height) && !utils.ObjectValid(imageOptions.width)) {
		if (imageOptions.aspect) {
			imageOptions.width = imageOptions.height*aspect;
		} else {
			imageOptions.width = image.width;
		}
	} else if (!utils.ObjectValid(imageOptions.width) && !utils.ObjectValid(imageOptions.height)) {
		imageOptions.width = image.width;
		imageOptions.height = image.height;
	}
	
	var width = imageOptions.width;
	var height = imageOptions.height;
	var x = 0;
	var y = 0;

//	myConsole.Log("Aspect: "+imageOptions.aspect+" "+aspect+" width: "+width+" height: "+height);
	
	if (imageOptions.aspect) {
		var oldWidth = image.width;
		var oldHeight = image.height;
		
		var newAspect = imageOptions.width/imageOptions.height;

		if (aspect > newAspect) { // Wider
//			myConsole.Log("Resizing height: "+imageOptions.height);
		
			image.height = imageOptions.height;
			image.width = image.height*aspect;
		} else {
//			myConsole.Log("Resizing width: "+imageOptions.width);

			image.width = imageOptions.width;
			image.height = image.width/aspect;
		}
		image.data = image.data.resize(image.width, image.height, "!");

		if (image.width > width) {
			x = parseInt((image.width/2)-(width/2));
		} else {
			x = parseInt((width/2)-(image.width/2));
		}
		if (image.height > height) {
			y = parseInt((image.height/2)-(height/2));
		} else {
			y = parseInt((height/2)-(image.height/2));
		}
		
//		myConsole.Log("Processing, aspect: "+aspect+" width: "+width+" height: "+height+" x: "+x+" y: "+y);

		image.data = image.data.crop(width, height, x, y);
	} else {
		image.data.resize(width, height, "!");	
	}

	image.width = width;
	image.height = height;

	if (utils.ObjectValid(imageOptions.pow2)) {
		if (utils.IsString(imageOptions.pow2)) {
			var intPow = parseInt(imageOptions.pow2);
			if (intPow <= 0) {
				imageOptions.pow2 = null;
			}
		} else if (!imageOptions.pow2) {
			imageOptions.pow2 = null;
		}
	}
	
	if (utils.ObjectValid(imageOptions.pow2)) {
//		myConsole.Log("Processing pow2: "+imageOptions.pow2);

		var largest = null;
		
		if (utils.IsString(imageOptions.pow2)) {
			var specified = parseInt(imageOptions.pow2);
		
			var pow = 2;
			while (true) {
				var newPow = pow*2;
				if (newPow >= specified) {
					var diffBigger = newPow-specified;
					var diffSmaller = specified-pow;
					
					if (diffBigger < diffSmaller) {
						largest = newPow;
					} else {
						largest = pow;					
					}
					break;
				}
				pow = newPow;
			}
		} else if (imageOptions.pow2 > 0) {
			largest = image.width;

			var testHeight = null;
			if (utils.ObjectValid(imageOptions.height)) {
				testHeight = imageOptions.height;
			} else {
				testHeight = image.height;
			}

			if (testHeight > largest) {
				largest = testHeight;
			}
		}

		if (utils.ObjectValid(largest)) {
			var pow = 2;
			while (true) {
				pow *= 2;
				if (pow >= largest) {
					break;
				}
			}

//			myConsole.Log("Processing pow2, largest: "+largest+" pow: "+pow);

			image.data.resize(pow, pow, "!");
			image.width = largest;
			image.height = largest;		
		} else {
			imageOptions.pow2 = null;
		}
	}
	
	var outputType = "JPG";
	if (utils.ObjectValid(imageOptions.output)) {
		outputType = imageOptions.output.toUpperCase();
	}
	
//	myConsole.Log("Processing, type: "+outputType);
	
	try {
		image.data = image.data.toBuffer(outputType, function (err, buffer) {
			if (err) {
				if (utils.ObjectValid(completeFunc)) {
					completeFunc(false);
				}
				return;
			}

			var contentType = null;
			if (utils.ObjectValid(theImage.contentType)) {
				contentType = theImage.contentType;
			} else {
				contentType = "JPEG";
				if (theImage.url.indexOf("png") > -1) {
					contentType = "PNG";
				} else if (theImage.url.indexOf("gif") > -1) {
					contentType = "GIF"
				} else if ((theImage.url.indexOf("jpg") <= -1) && (theImage.url.indexOf("jpeg") <= -1)) {
					var index = theImage.url.lastIndexOf(".");
					var extension = theImage.url.substring(index);
					contentType = extension.toUpperCase();
				}
		
				contentType = "image/"+contentType;
			}

			europeana.SendResponse(response, {status: europeana.ERRORS.NONE, contentType: contentType, data: buffer});
			if (utils.ObjectValid(completeFunc)) {
				completeFunc(true);
			}
		});
	} catch (error) {
		myConsole.Log("Processing toBuffer error! "+error.message);
		if (utils.ObjectValid(completeFunc)) {
			completeFunc(false);
		}
	}
};

Europeana.prototype.RequestAndGetImagesInPage = function(visited, pageUrl, domain, onComplete) {
//	myConsole.Log("RequestAndGetImagesInPage "+pageUrl);

	var completeFunc = onComplete;
	var theDomain = domain;

	europeana.VisitedURL(visited, pageUrl);

	var visitedList = visited;

	var pageRequest = request({
		uri: pageUrl,
	}, function(error, response, body) {
		var $ = cheerio.load(body);

		europeana.GetImagesInPage(visitedList, cheerio.load(body), theDomain, function(images) {
			if (utils.ObjectValid(completeFunc)) {
				completeFunc(images);
			}
		});
	});
	
	return pageRequest;
};

Europeana.prototype.GetImagesInPage = function(visited, body, domain, onComplete) {
//	myConsole.Log("GetImagesInPage "+domain);

	var $ = body;
	var theDomain = domain;
	var completeFunc = onComplete;

	var visitedList = visited;

	var images = $("img");
	if (images.length <= 0) {
		myConsole.Log("GetImagesInPage no images in page");
		if (utils.ObjectValid(completeFunc)) {
			completeFunc(null);
		}
	} else {
		var imagesData = new Array();

//		myConsole.Log("GetImagesInPage num images: "+images.length);
	
		var imageIndex = 0;
		images.each(function() {
			var img = $(this);
			var src = img.attr("src");
			if (!utils.ObjectValid(src) || src.length <= 0) {
				imageIndex++;
			
				if (imageIndex >= images.length) {
					if (utils.ObjectValid(completeFunc)) {
						completeFunc(imagesData);
					}
				}
				return;
			}
			
			var fullAddress = europeana.GetAbsoluteURL(theDomain, src);
		
			europeana.GetImage(visitedList, fullAddress, false, function(image) {
				if (utils.ObjectValid(image)) {
					imagesData.push(image);
				}
			
				imageIndex++;
			
				if (imageIndex >= images.length) {
					if (utils.ObjectValid(completeFunc)) {
						completeFunc(imagesData);
					}
				}
			});
		});
	}
};

Europeana.prototype.FinishRecordImage = function(response, images, imageOptions, onChecked) {
//	myConsole.Log("FinishRecordImage");

	if (!utils.ObjectValid(images) || images.length <= 0) {
		var message = "Couldn't get any images!";
		myConsole.Log("****** FinishRecordImage ****** ERROR: "+message); 
		if (utils.ObjectValid(onChecked)) {
			onChecked(null);
		} else {
			europeana.SendResponse(response, {status: europeana.ERRORS.NONE_FOUND, contentType: europeana.MIME.TEXT, data: message});
		}
		return;
	}

	var largest = new Object;
	largest.image = null;
	largest.size = 0;

//	myConsole.Log("Num arrays: "+images.length);
	for (var j = 0; j < images.length; j++) {
		var imagesArray = images[j];
//		myConsole.Log("Images: "+imagesArray.length);

		for (var i = 0; i < imagesArray.length; i++) {
			var test = imagesArray[i];
//			myConsole.Log("Image: "+test);
	
			if (!utils.ObjectValid(test)) {
				continue;
			}
	
//			myConsole.Log("Got image to test with url: "+test.url);

			if (utils.ObjectValid(imageOptions.minWidth)) {
				if (test.width < imageOptions.minWidth) {
					continue;
				}
			}
			if (utils.ObjectValid(imageOptions.minHeight)) {
				if (test.height < imageOptions.minHeight) {
					continue;
				}
			}

			var size = test.width*test.height;
			if (size > largest.size) {
				largest.image = test;
				largest.size = size;
			}
		}
	}

	if (!utils.ObjectValid(largest.image)) {
		var message = "Tried to get the largest image in the page but the largest image is somehow invalid! It may not have met the minimum size requirements.";
		myConsole.Log("FinishRecordImage error: "+message);
		if (utils.ObjectValid(onChecked)) {
			onChecked(null);
		} else {
			europeana.SendResponse(response, {status: europeana.ERRORS.NONE_FOUND, contentType: europeana.MIME.TEXT, data: message});
		}
		return;
	}
	
	if (utils.ObjectValid(onChecked)) {
		onChecked(largest.image);
		return;
	}
	
	europeana.ProcessImageAndRespond(response, largest.image, imageOptions, function(success) {
		if (!success) {
			var message = "Tried to get the largest image in the page but something went wrong!";
			myConsole.Log("FinishRecordImage error: "+message);
			europeana.SendResponse(response, {status: europeana.ERRORS.RETRIEVING, contentType: europeana.MIME.TEXT, data: message});
		}
	});
};

Europeana.prototype.GetRecordImage = function(currentDomain, visited, response, recordId, imageOptions, onChecked) {
//	myConsole.Log("GetRecordImage "+recordId);

	var theRes = response;
	var theId = recordId;
	var optionsForImage = imageOptions;

	var visitedList = visited;
	
	var theDomain = currentDomain;

	var checkedFunc = onChecked;

	var queryString = europeana.APIURL+"/record"+recordId+".json?wskey="+europeana.KEY+"&profile="+europeana.PROFILES.RECORD;
	europeana.Query(queryString, function(response, data) {
//		myConsole.Log("GetRecordImage got data: "+data);
	
		var resultObject = null;
		try {
			resultObject = JSON.parse(data);
		} catch (error) {
			myConsole.Log("GetRecordImage error parsing JSON: "+data+" with query: "+queryString);
			europeana.FinishRecordImage(theRes, null, optionsForImage, checkedFunc);
			return;		
		}

		var item = europeana.GetObjectData("minimal", theId, resultObject);
		if (utils.ObjectValid(item.unretrievable)) {
			if (utils.ObjectValid(checkedFunc)) {
				checkedFunc(null);
			} else {
				europeana.CantGetAsset(theRes, item.unretrievable);
			}
			return;
		}
		
		if (item.type.toLowerCase() !== "image") {
			myConsole.Log("GetRecordImage image type not correct: "+image.type+" with query: "+queryString);
			europeana.FinishRecordImage(theRes, null, optionsForImage, checkedFunc);
			return;		
		}
		
		var visitedList = visited;
		
		europeana.GetImage(visitedList, item.url, false, function(image) {

			var siteImages = new Array();
			
			if (utils.ObjectValid(image)) {
				var pageImages = new Array();
				pageImages.push(image);
				siteImages.push(pageImages);
				
//				console.log("Got image with item.url "+item.url);
			} else {
//				console.log("Did not get image with item.url "+item.url);
			}
			
			europeana.GetImage(visitedList, item.object, false, function(image) {

				if (utils.ObjectValid(image)) {
					var objectImages = new Array();
					objectImages.push(image);
					siteImages.push(objectImages);

//					console.log("Got image with item.object "+item.object);
				} else {
//					console.log("Did not get image with item.object "+item.object);
				}
				
				if (!utils.ObjectValid(item.page)) {
					europeana.FinishRecordImage(theRes, siteImages, optionsForImage, checkedFunc);
				} else {
			
//					myConsole.Log("Scraping page "+item.page);

					europeana.VisitedURL(visitedList, item.page);

					var imageRequest = request({
						uri: item.page,
					}, function(error, response, body) {
						body = cheerio.load(body);
						var $ = body;

						var domain = europeana.GetBaseURLOfPage(item.page, body, response);
						theDomain = europeana.SetCurrentDomain(domain);
				
//						myConsole.Log("Domain: "+domain);
				
//						myConsole.Log("Getting images in page...");
						europeana.GetImagesInPage(visitedList, body, domain, function(images) {
//							myConsole.Log("Got images in page: "+images);
				
							// Got images in the page here...
							if (utils.ObjectValid(images) && images.length > 0) {
								siteImages.push(images);
							}
					
							var frames = $("frame");
							if (frames.length <= 0) {
								europeana.FinishRecordImage(theRes, siteImages, optionsForImage, checkedFunc);
							} else {
								var framesImages = new Array();
				
								frames.each(function() {
									var frame = $(this);
					
									var src = frame.attr("src");
					
									var address = null;
									if (src.indexOf("http") === 0) {
										address = src;
									} else if (utils.ObjectValid(domain)) {
										address = domain+"/"+src;
									}
					
									var frameRequest = europeana.RequestAndGetImagesInPage(visitedList, address, domain, function(imagesData) {
										framesImages.push(imagesData);
										if (framesImages.length >= frames.length) {
											for (var f = 0; f < framesImages.length; f++) {
												var images = framesImages[f];
												siteImages.push(images);
											}
											
											europeana.FinishRecordImage(theRes, siteImages, optionsForImage, checkedFunc);
										}
									});
								});
							}
						});
					});
				}
			});
		});
		
	}, function(error) {
		var message = "Problem with request: "+error.message;
		myConsole.Log("GetRecordImage error: "+message);
		europeana.SendResponse(theRes, {status: europeana.ERRORS.BAD_DATA, contentType: europeana.MIME.TEXT, data: message});
	});
};
