var fs = require("fs");
eval(fs.readFileSync("myconsole-1.0.js")+"");
eval(fs.readFileSync("utils-1.03.js")+"");

var request = require("request");
var url = require("url");
var http = require("http");
var https = require("https");
var cheerio = require("cheerio");
var sizeOf = require("image-size");
var gm = require("gm");

function Europeana() {
	this.version = "1";

	this.currentDomain = null;

	this.IP = "82.165.11.100";
	this.PORT = 9001;

	this.KEY = "UF5WiHbC5";
	this.HOSTNAME = "europeana.eu";
	this.APIURL = "/api/v2";
	this.REUSABILITY = "open";
	
	this.PROFILES = new Object;
	this.PROFILES.RECORDS = "standard";
	this.PROFILES.RECORD = "params";
	
	this.EXTENSIONS = new Object;
	this.EXTENSIONS.AUDIO = ["mp3", "mp4", "ogg", "wav", "wave", "aif", "aiff"]; 
	this.EXTENSIONS.VIDEO = ["mp4", "wmv", "flv"];
	this.EXTENSIONS.IMAGE = ["jpg", "jpeg", "png", "gif", "tiff"];
	
	this.ERRORS = new Object;
	this.ERRORS.NONE = 0;
	this.ERRORS.NO_RESULTS = 1;
	this.ERRORS.NONE_FOUND = 2;
	this.ERRORS.REQUEST = 3;
	this.ERRORS.BAD_DATA = 4;
	this.ERRORS.NO_TERMS = 5;
	this.ERRORS.NO_TYPE = 6;
	this.ERRORS.CANCELLED = 7;
	this.ERRORS.BAD_ARCHIVE = 8;
	this.ERRORS.RETRIEVING = 9;
	
	this.MIME = new Object;
	this.MIME.JSON = "application/json";
	this.MIME.TEXT = "text/plain";
	
	this.BLACKLIST = new Object;
	this.BLACKLIST.PROVIDERS = [{value: "Digitale Collectie", reason: "Registration required"}];
	this.BLACKLIST.DOMAINS = [{value: "cvce.eu", reason: "Embedded and protected content"},
														{value: "ena.lu", reason: "Forwards to cvce.eu - embedded and protected content"},
														{value: "kug.ac.at", reason: "Registration required"},
														{value: "clip.jccm.es", reason: "All assets seem to be unavailable"},
														{value: "de.sevenload.com", reason: "Domain won't resolve"},
														{value: "youtube.com", reason: "Can't extract videos from YouTube"},
														{value: "collectie.groningermuseum.nl", reason: "Can't scrape content"}];
};

eval(fs.readFileSync("sound.js")+"");
eval(fs.readFileSync("image.js")+"");
eval(fs.readFileSync("video.js")+"");

Europeana.prototype.ContentBlacklisted = function(type, value) {
//	myConsole.Log("ContentBlacklisted type: "+type+" value: "+value);

	var unretrievable = null;
	var checkArray = null;
	
	if (type === "provider") {
		checkArray = europeana.BLACKLIST.PROVIDERS;
	} else if (type === "domain") {
		checkArray = europeana.BLACKLIST.DOMAINS;
	}
	
	if (!utils.ObjectValid(checkArray)) {
		return unretrievable;
	}
	
	for (var b = 0; b < checkArray.length; b++) {
		var item = checkArray[b];
//		myConsole.Log("Checking "+value+" against "+item.value);
		
		if (value.toLowerCase().indexOf(item.value.toLowerCase()) > -1) {
//			myConsole.Log("Match!");
			unretrievable = new Object;
			unretrievable.value = item.value;
			unretrievable.reason = item.reason;
			unretrievable.type = type;
			break;
		} else {
//			myConsole.Log("No match");
		}
	}
	
	return unretrievable;
};

Europeana.prototype.VisitedURL = function(list, theUrl) {
	if (europeana.HaveVisitedURL(list, theUrl)) {
		return;
	}
	list.push(theUrl);
};

Europeana.prototype.HaveVisitedURL = function(list, theUrl) {
	var index = list.indexOf(theUrl);
	if (index > -1) {
		return true;
	}
	return false;
};

Europeana.prototype.CanRecursivelyCheckURL = function(currentDomain, visited, theUrl) {
	if (europeana.HaveVisitedURL(visited, theUrl)) {
		console.log("CanRecursivelyCheckURL visited URL before: "+theUrl);
		return false;
	}
	if (theUrl.indexOf("#") > -1) {
		console.log("CanRecursivelyCheckURL is to an anchor: "+theUrl);
		return false;
	}
	if (utils.ObjectValid(currentDomain) && theUrl.indexOf(currentDomain) <= -1) {
		console.log("CanRecursivelyCheckURL does not contain current domain: "+currentDomain+" url: "+theUrl);
		return false;
	}
	console.log("Can visit URL! "+theUrl);
	return true;
};

Europeana.prototype.SetCurrentDomain = function(domain) {
	if (!utils.ObjectValid(domain)) {
		return;
	}

	var firstBrackets = "://";
	var index = domain.indexOf(firstBrackets);
	if (index > -1) {
		domain = domain.substring(index+firstBrackets.length);
	}
	
	index = domain.indexOf("/");
	if (index > -1) {
		domain = domain.substring(0, index);
	}
	
//	console.log("*** SET CURRENT DOMAIN: "+domain+" ***");
//	europeana.currentDomain = domain;
	return domain;
};

Europeana.prototype.GetAbsoluteURL = function(domain, path) {	
	var absolute = null;
	if (path.indexOf("http") === 0) {
		absolute = path;
	} else if (utils.ObjectValid(domain)) {
		absolute = europeana.AppendURLToDomain(domain, path);
	} else {
		absolute = path;
	}
	
	return absolute;
};

Europeana.prototype.AppendURLToDomain = function(domain, path) {
	if (!utils.ObjectsValid(domain, path) || domain.length <= 0 || path.length <= 0) {
		return null;
	}

//	console.log("AppendURLToDomain begin domain: "+domain+" path: "+path);
	
	if (path[0] !== "/") {
		var newAddress = domain+path;
//		console.log("AppendURLToDomain A end: "+newAddress);
		return newAddress;
	}

	var working = domain;

	var prefix = "http://"
	var index = domain.indexOf(prefix);
	if (index <= -1) {
		prefix = "https://";
		index = domain.indexOf(prefix);
	}
	if (index > -1) {
		working = domain.substring(prefix.length);
	} else {
		prefix = "";
	}

	index = working.indexOf("/");
	if (index > -1) {
		working = working.substring(0, index);	
	}

	if (path[0] !== "/") {
		path = "/"+path;
	}

	var newAddress = prefix+working+path;

//	console.log("AppendURLToDomain B end: "+newAddress);
	return newAddress;
};

Europeana.prototype.GetBaseURLOfPage = function(address, dom, response) {
//	myConsole.Log("GetBaseURLOfPage "+address);

	var $ = dom;

	var domain = null;
	var base = $("base");
	if (utils.ObjectValid(base) && base.length > 0) {
		var baseAddress = base.attr("href");
		if (utils.ObjectValid(baseAddress) && baseAddress.length > 0) {
			domain = baseAddress;
		}
	}
	
	var protocol = "http";
	if (address.indexOf("https") === 0) {
		protocol += s;
	}

	if (!utils.ObjectValid(domain)) {
		var domain = protocol+"://"+response.req._headers.host+response.req.path;
	
		var index = domain.lastIndexOf("/");
		if (index >= 0) {
			domain = domain.substring(0, index+1);
		}
	}
	
	if (!utils.ObjectValid(domain)) {
		if (utils.ObjectsValid([response, response.req, response.req._headers, response.req._headers.host])) {
			var newPath = "/";
			var setCookie = response.headers["set-cookie"];
			if (utils.ObjectValid(setCookie)) {
				for (var i = 0; i < setCookie.length; i++) {
					var data = setCookie[i];
					var findString = "path=";
					var index = data.indexOf(findString);
					if (index > -1) {
						var path = data.substring(index+(findString.length));
						index = path.indexOf(";");
						if (index > -1) {
							path = path.substring(0, index);
						}
						
						if (path.length > newPath.length) {
							newPath = path;
						}
					}
				}
			}
	
			domain = protocol+"://"+response.req._headers.host+newPath;
		}
	}
	
	return domain;
};

Europeana.prototype.SendResponse = function(response, options) {
	if (!utils.ObjectValid(options)) {
		options = new Object;
	}
	if (!utils.ObjectValid(options.contentType)) {
		options.contentType = "text/plain";
	}
	if (!utils.ObjectValid(options.status)) {
		options.status = europeana.ERRORS.NONE;
	}

	var header = new Object;
	if (utils.ObjectValid(options.contentType)) {
		header["Content-Type"] = options.contentType;
	}
	if (utils.ObjectValid(options.location)) {
		header["Location"] = options.location;
	}
	header["Warning"] = ""+options.status;

	myConsole.Log("SendResponse status: "+options.status);
	
	if (utils.ObjectValid(options.message)) {
		myConsole.Log("SendResponse message: "+options.message);
	}

	response.writeHead(200, header);
	response.end(options.data);
};

Europeana.prototype.GetObjectData = function(profile, id, resultObject) {

	var item = new Object;
	item.id = id;
	item.url = null;
	item.page = null;
	item.object = null;
	item.type = "unknown";
	item.unretrievable = null;

	if (profile === "full") {
		item.title = null;
		item.attribution = new Object;
		item.attribution.credit = null;
		item.attribution.provider = null;
		item.description = null;
	}
	
	if (utils.ObjectValid(resultObject)) {
	
//		utils.LogProperties(resultObject);
	
		if (utils.ObjectValid(resultObject.success) && !resultObject.success) {
			item.unretrievable = new Object;
			item.unretrievable.value = "";
			item.unretrievable.reason = resultObject.error;
		} else {
			var checkDomain = null;
			if (utils.ObjectValid(resultObject.edmIsShownAt)) {
				item.page = ""+resultObject.edmIsShownAt;
				if (utils.ObjectValid(item.page)) {
					checkDomain = item.page;
				}
			}
	
			if (utils.ObjectValid(resultObject.object)) {
				if (utils.ObjectValid(resultObject.object.type)) {
					item.type = resultObject.object.type.toLowerCase();
				}

				if (utils.ObjectValid(resultObject.object.aggregations)) {
					if (!utils.ObjectValid(item.page) && utils.ObjectValid(resultObject.object.aggregations[0].edmIsShownAt)) {
						item.page = resultObject.object.aggregations[0].edmIsShownAt;
						if (utils.ObjectValid(item.page)) {
							checkDomain = item.page;
						}
					}
					if (utils.ObjectValid(resultObject.object.aggregations[0].edmIsShownBy)) {
						item.url = resultObject.object.aggregations[0].edmIsShownBy;
						if (utils.ObjectValid(item.url)) {
							checkDomain = item.url;
						}
					}
					if (utils.ObjectValid(resultObject.object.aggregations[0].edmObject)) {
						item.object = resultObject.object.aggregations[0].edmObject;
						if (utils.ObjectValid(item.object)) {
							checkDomain = item.object;
						}
					}
				}
			}

			if (utils.ObjectValid(checkDomain)) {
				item.unretrievable = europeana.ContentBlacklisted("domain", checkDomain);
			}

			var checkProvider = null;
			if (utils.ObjectValid(resultObject.dataProvider)) {
				checkProvider = ""+resultObject.dataProvider;
			}
	
			if (utils.ObjectValid(resultObject.object)) {
				if (utils.ObjectValid(resultObject.object.aggregations)) {
					if (!utils.ObjectValid(checkProvider) && utils.ObjectValid(resultObject.object.aggregations[0].edmDataProvider.def)) {
						checkProvider = resultObject.object.aggregations[0].edmDataProvider.def[0];
					}
					if (profile === "full") {
						if (!utils.ObjectValid(item.attribution.credit)) {
							if (utils.ObjectValid(resultObject.object.aggregations[0].dcRights)) {
								if (utils.ObjectValid(resultObject.object.aggregations[0].dcRights.en)) {
									if (utils.IsArray(resultObject.object.aggregations[0].dcRights.en)) {
										item.attribution.credit = resultObject.object.aggregations[0].dcRights.en[0];
									} else {
										item.attribution.credit = resultObject.object.aggregations[0].dcRights.en;
									}
								} else if (utils.ObjectValid(resultObject.object.aggregations[0].dcRights.def)) {
									if (utils.IsArray(resultObject.object.aggregations[0].dcRights.def)) {
										item.attribution.credit = resultObject.object.aggregations[0].dcRights.def[0];
									} else {
										item.attribution.credit = resultObject.object.aggregations[0].dcRights.def;
									}
								}
							}
						}
					}
				}

				if (profile === "full") {
					if (utils.ObjectValid(resultObject.object.title)) {
						if (utils.IsArray(resultObject.object.title)) {
							item.title = resultObject.object.title[0];
						} else {
							item.title = resultObject.object.title;
						}
					}

					if (utils.ObjectValid(resultObject.object.proxies)) {
						if (!utils.ObjectValid(item.title) && utils.ObjectValid(resultObject.object.proxies[0].dcTitle)) {
							if (utils.ObjectValid(resultObject.object.proxies[0].dcTitle.en)) {
								if (utils.IsArray(resultObject.object.proxies[0].dcTitle.en)) {
									item.title = resultObject.object.proxies[0].dcTitle.en[0];
								} else {
									item.title = resultObject.object.proxies[0].dcTitle.en;
								}
							} else if (utils.ObjectValid(resultObject.object.proxies[0].dcTitle.def)) {
								if (utils.IsArray(resultObject.object.proxies[0].dcTitle.def)) {
									item.title = resultObject.object.proxies[0].dcTitle.def[0];
								} else {
									item.title = resultObject.object.proxies[0].dcTitle.def;
								}
							}
						}
					
						if (utils.ObjectValid(resultObject.object.proxies[0].dcDescription)) {
							if (utils.ObjectValid(resultObject.object.proxies[0].dcDescription.en)) {
								if (utils.IsArray(resultObject.object.proxies[0].dcDescription.en)) {
									item.description = resultObject.object.proxies[0].dcDescription.en[0];
								} else {
									item.description = resultObject.object.proxies[0].dcDescription.en;
								}
							} else if (utils.ObjectValid(resultObject.object.proxies[0].dcDescription.def)) {
								if (utils.IsArray(resultObject.object.proxies[0].dcDescription.def)) {
									item.description = resultObject.object.proxies[0].dcDescription.def[0];
								} else {
									item.description = resultObject.object.proxies[0].dcDescription.def;
								}
							}
						}

						if (!utils.ObjectValid(item.creator)) {
							if (utils.ObjectValid(resultObject.object.proxies[0].dcRights)) {
								if (utils.ObjectValid(resultObject.object.proxies[0].dcRights.en)) {
									if (utils.IsArray(resultObject.object.proxies[0].dcRights.en)) {
										item.attribution.credit = resultObject.object.proxies[0].dcRights.en[0];
									} else {
										item.attribution.credit = resultObject.object.proxies[0].dcRights.en;
									}
								} else if (utils.ObjectValid(resultObject.object.proxies[0].dcRights.def)) {
									if (utils.IsArray(resultObject.object.proxies[0].dcRights.def)) {
										item.attribution.credit = resultObject.object.proxies[0].dcRights.def[0];
									} else {
										item.attribution.credit = resultObject.object.proxies[0].dcRights.def;
									}
								}
							}
						}
					}

					if (utils.ObjectValid(resultObject.object.agents)) {
						if (utils.ObjectValid(resultObject.object.agents[0].prefLabel.def)) {
							if (utils.IsArray(resultObject.object.agents[0].prefLabel.def)) {
								item.attribution.credit = resultObject.object.agents[0].prefLabel.def[0];
							} else {
								item.attribution.credit = resultObject.object.agents[0].prefLabel.def;
							}
							if (utils.IsArray(item.attribution.credit)) {
								item.attribution.credit	= item.attribution.credit[0];
							}
						}
					}
				}
			}
		
			if (utils.ObjectValid(checkProvider) && !utils.ObjectValid(item.unretrievable)) {
				item.unretrievable = europeana.ContentBlacklisted("provider", checkProvider);
			}
			if (utils.ObjectValid(item.attribution)) {
				item.attribution.provider = checkProvider;
			}
		}
	}
	
	if (!utils.ObjectValid(item.page)) {
		if (utils.ObjectValid(item.url)) {
			item.page = item.url;
			item.url = null;
		} else if (utils.ObjectValid(item.object)) {
			item.page = item.object;
			item.object = null;
		}
	}
	
	return item;
};

Europeana.prototype.CheckRecordsAvailabilityRecursively = function(mediaType, currentDomain, visited, res, items, index, newRecords, numRecords, availableOnly, imageOptions, onComplete) {

	if (!utils.ObjectValid(newRecords)) {
		newRecords = new Array();
	}

	index++;
	if (index >= items.length) {
		if (utils.ObjectValid(onComplete)) {
			onComplete(newRecords);
		}
		return;
	}

	var optionsForImages = imageOptions;
	var visitedList = visited;
	var theDomain = currentDomain;
	var media = mediaType;
	
	var item = items[index];
	if (!utils.ObjectValid(item)) {
		if (utils.ObjectValid(onComplete)) {
			onComplete(newRecords);
		}
		return;
	}
	
	item = europeana.GetObjectData("minimal", item.id, item);
	if (availableOnly && utils.ObjectValid(item.unretrievable)) {
		// Do nothing
	} else {
		if (availableOnly) {
			var theRes = res;
			var theItems = items;
			var theIndex = index;
			var theRecords = newRecords;
			var maxRecords = numRecords;
			var onlyAvailable = availableOnly;
			var completeFunc = onComplete;

			europeana.GetRecord(theDomain, visitedList, res, item.id, function(statusCode, item) {
				myConsole.Log("GOT RECORD statusCode: "+statusCode+" media: "+media+" optionsForImages: "+optionsForImages);

				if (statusCode == 200 && !utils.ObjectValid(item.unretrievable)) {
				
					if (media.toLowerCase() === "image" && utils.ObjectValid(optionsForImages)) {
						var theItem = item;
					
						europeana.GetRecordImage(theDomain, visitedList, res, item.id, optionsForImages, function(image) {
							var keepGoing = true;
							if (utils.ObjectValid(image)) {
								theRecords.push(theItem);
								if (theRecords.length >= maxRecords) {
									if (utils.ObjectValid(onComplete)) {
										keepGoing = false;
										completeFunc(theRecords);
									}
								}
							}

							if (keepGoing) {
								europeana.CheckRecordsAvailabilityRecursively(media, theDomain, visitedList, theRes, theItems, theIndex, theRecords, maxRecords, onlyAvailable, optionsForImages, completeFunc);
							}
						});
					} else {
						var keepGoing = true;
						theRecords.push(item);
						if (theRecords.length >= maxRecords) {
							if (utils.ObjectValid(onComplete)) {
								keepGoing = false;
								completeFunc(theRecords);
							}
						}
						
						if (keepGoing) {
							europeana.CheckRecordsAvailabilityRecursively(media, theDomain, visitedList, theRes, theItems, theIndex, theRecords, maxRecords, onlyAvailable, optionsForImages, completeFunc);
						}
					}
				}
			});
			return;
		} else {
			newRecords.push(item);
			if (newRecords.length >= numRecords) {
				if (utils.ObjectValid(onComplete)) {
					onComplete(newRecords);
				}
				return;
			}
		}
	}

	europeana.CheckRecordsAvailabilityRecursively(media, theDomain, visitedList, res, items, index, newRecords, numRecords, availableOnly, optionsForImages, onComplete);
};

Europeana.prototype.GetRecordIDs = function(currentDomain, visited, response, searchTerms, mediaType, startIndex, numRecords, availableOnly, records, imageOptions) {
	myConsole.Log("GetRecordIDs searchTerms "+searchTerms+" mediaType "+mediaType+" startIndex "+startIndex+" numRecords "+numRecords+" availableOnly "+availableOnly+" records "+records+" imageOptions "+imageOptions);

	if (!utils.ObjectValid(records)) {
		records = new Array();
	}

	searchTerms = searchTerms.replace(" ","+");

	var queryString = europeana.APIURL+"/search.json?wskey="+europeana.KEY+"&query="+searchTerms+"&start="+startIndex+"&rows="+numRecords+"&profile="+europeana.PROFILES.RECORDS+"&reusability="+europeana.REUSABILITY;
	
	if (utils.ObjectValid(mediaType)) {
		queryString += "&qf=TYPE:"+mediaType.toUpperCase();
	}
	
	var theRes = response;
	var terms = searchTerms;
	var media = mediaType;
	var firstIndex = startIndex;
	var maxRecords = numRecords;
	var onlyAvailable = availableOnly;
	var optionsForImages = imageOptions;

	var theRecords = records;
	var visitedList = visited;
	var theDomain = currentDomain;

	europeana.Query(queryString, function(response, data) {
		var resultObject = JSON.parse(data);
		var items = null;
		if (utils.ObjectValid(resultObject)) {
			items = resultObject.items;
		}
		
		if (!utils.ObjectValid(items)) {
			items = new Array();
		}
		
		if (items.length <= 0) {
			europeana.SendResponse(theRes, {status: europeana.ERRORS.NO_RESULTS, contentType: europeana.MIME.TEXT, data: "No records found from your search parameters"});
			return;
		}
		
		var theResponse = response;
	
		europeana.CheckRecordsAvailabilityRecursively(media, theDomain, visitedList, theRes, items, -1, null, maxRecords, onlyAvailable, optionsForImages, function(finalRecords) {

			if (finalRecords.length < maxRecords) {
				europeana.GetRecordIDs(theDomain, visitedList, theRes, terms, media, firstIndex+maxRecords, maxRecords, onlyAvailable, finalRecords, optionsForImages);
				return;
			}
		
			var jsonResponse = "{\"items\":[";
	
			if (utils.ObjectValid(finalRecords)) {
				for (var i = 0; i < finalRecords.length; i++) {
					var item = finalRecords[i];
		
					jsonResponse += "\""+item.id+"\"";
					if (i < resultObject.items.length-1) {
						jsonResponse += ",";
					}
				}
			}		
	
			jsonResponse += "]}";
			
			var returnCode = europeana.ERRORS.NONE;
			if (finalRecords.length <= 0) {
				returnCode = europeana.ERRORS.NONE_FOUND;
			}
	
			europeana.SendResponse(theRes, {status: returnCode, contentType: europeana.MIME.JSON, data: jsonResponse});
		});
	}, function(error) {
		europeana.SendResponse(theRes, {status: europeana.ERRORS.BAD_DATA, contentType: europeana.MIME.TEXT, data: "Problem with request: "+error.message});
	});
};

Europeana.prototype.GetRecord = function(currentDomain, visited, response, recordId, onComplete) {
	var theRes = response;
	var theId = recordId;
	var completeFunc = onComplete;

	var queryString = europeana.APIURL+"/record"+recordId+".json?wskey="+europeana.KEY+"&profile="+europeana.PROFILES.RECORD;
	myConsole.Log("Query: "+queryString);
	europeana.Query(queryString, function(response, data) {
		var resultObject = JSON.parse(data);

		var item = europeana.GetObjectData("full", theId, resultObject);

		if (completeFunc) {
			completeFunc(response.statusCode, item);
		} else {
			var jsonResponse = JSON.stringify(item);
			
			europeana.SendResponse(theRes, {status: europeana.ERRORS.NONE, contentType: europeana.MIME.JSON, data: jsonResponse});
		}
	}, function(error) {
		var statusCode = 500;
		var message = "Problem with request: "+error.message;
	
		if (completeFunc) {
			completeFunc(statusCode, message);
		} else {
			europeana.SendResponse(theRes, {status: europeana.ERRORS.BAD_DATA, contentType: europeana.MIME.TEXT, data: message});
		}
	});
};

Europeana.prototype.Query = function(query, onComplete, onFailure) {
	myConsole.Log("Query: "+query);

	var completeFunc = onComplete;
	var failureFunc = onFailure;

	var options = {
		hostname: europeana.HOSTNAME,
		port: 80,
		path: query,
		method: "GET"
	};
	
	var request = http.request(options, function(res) {
		var requestResponse = res;

		requestResponse.setEncoding('utf8');

		var responseString = "";

		requestResponse.on('data', function(data) {
			responseString += data;
		});

		requestResponse.on('end', function() {
			myConsole.Log("Query, end: "+responseString);
			if (utils.ObjectValid(completeFunc)) {
				completeFunc(requestResponse, responseString);
			}
		});
	});

	request.on('error', function(error) {
		if (utils.ObjectValid(failureFunc)) {
			failureFunc(error);
		}
	});
	
	request.end();
};

Europeana.prototype.CheckURLExists = function(theUrl, onComplete) {
	if (!utils.ObjectValid(onComplete)) {
		return;
	}

	var completeFunc = onComplete;

	request({
		uri: theUrl,
	}, function(error, response, body) {
		var exists = false;
		if (response.statusCode === 200) {
			exists = true;
		}
		
		completeFunc(exists, error, response, body);
	});
};

Europeana.prototype.CantGetAsset = function(response, unretrievable) {
	unretrievable.message = "The content could not be retrieved from the archive that provides this asset";
	
	var jsonData = JSON.stringify(unretrievable);
	europeana.SendResponse(response, {status: europeana.ERRORS.BAD_ARCHIVE, contentType: europeana.MIME.JSON, data: jsonData});
};

var europeana = new Europeana();

http.createServer(function (req, res) {

	var url = require('url');
	var url_parts = url.parse(req.url, true);
	var query = url_parts.query;
	
	if (req.url.indexOf("/crossdomain.xml") == 0) {
		var crossDomainXML = "<?xml version=\"1.0\"?><cross-domain-policy><allow-access-from domain=\"*\"/></cross-domain-policy>";
		res.end(crossDomainXML);
		return;
	}

	var type = "";
	var search = "";
	var media = null;
	var first = null;
	var count = null;
	var id = "";
	var availableOnly = false;
	var output = null;
	var imageOptions = new Object;
	imageOptions.width = null;
	imageOptions.height = null;
	imageOptions.aspect = false;
	imageOptions.pow2 = false;
	imageOptions.crop = null;

	imageOptions.minWidth = null;
	imageOptions.minHeight = null;

	for(var propt in query) {
		try {
			if (propt === "type") {
				type = query[propt].toLowerCase();
			} else if (propt === "search") {
				search = query[propt];
			} else if (propt === "media") {
				media = query[propt].toLowerCase();
			} else if (propt === "id") {
				id = query[propt];
			} else if (propt === "first") {
				first = parseInt(query[propt]);
			} else if (propt === "count") {
				count = parseInt(query[propt]);
			} else if (propt === "availableOnly") {
				availableOnly = query[propt];
				if (availableOnly === "true") {
					availableOnly = true;
				} else {
					availableOnly = false;
				}
			} else if (propt === "width") {
				imageOptions.width = parseInt(query[propt]);
			} else if (propt === "height") {
				imageOptions.height = parseInt(query[propt]);
			} else if (propt === "aspect") {
				imageOptions.aspect = query[propt];
				if (imageOptions.aspect === "true") {
					imageOptions.aspect = true;
				} else {
					imageOptions.aspect = false;
				}
			} else if (propt === "pow2") {
				imageOptions.pow2 = query[propt];
				if (imageOptions.pow2 === "true") {
					imageOptions.pow2 = true;
				} else if (imageOptions.pow2 === "false") {
					imageOptions.pow2 = false;
				}
			} else if (propt === "crop") {
				imageOptions.crop = parseInt(query[propt]);
			} else if (propt === "output") {
				output = query[propt].toLowerCase();
			} else if (propt === "minwidth") {
				imageOptions.minWidth = parseInt(query[propt]);
			} else if (propt === "minheight") {
				imageOptions.minHeight = parseInt(query[propt]);
			}
		} catch (error) {
			// Do nothing
		}
	}
	
	imageOptions.output = output;
	
	var visited = new Array();
	var currentDomain = null;
	
	if (type === "records") {
		if (!utils.ObjectValid(first) || first <= 0) {
			first = 1;
		}
		if (!utils.ObjectValid(count) || count <= 0) {
			count = 10;
		}
		europeana.GetRecordIDs(currentDomain, visited, res, search, media, first, count, availableOnly, null, imageOptions);
	} else if (type === "record") {
		europeana.GetRecord(currentDomain, visited, res, id);
	} else if (type === "image") {
		europeana.GetRecordImage(currentDomain, visited, res, id, imageOptions);
	} else if (type === "video") {
		europeana.GetRecordVideo(currentDomain, visited, res, id);
	} else if (type === "sound") {
		europeana.GetRecordSound(currentDomain, visited, res, id);
	}
}).listen(europeana.PORT, europeana.IP);

console.log("Server running at http://"+europeana.IP+":"+europeana.PORT+"/ version: "+europeana.version);