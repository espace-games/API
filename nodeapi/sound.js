Europeana.prototype.URLHasValidSoundExtension = function(fileUrl) {
	if (!utils.ObjectValid(fileUrl)) {
		return false;
	}

	for (var e = 0; e < europeana.EXTENSIONS.AUDIO.length; e++) {
		var extension = europeana.EXTENSIONS.AUDIO[e];
		fileUrl = fileUrl.toLowerCase();
		
		if (fileUrl.indexOf("."+extension) > -1) {
//			console.log("**** URL HAS VALID EXTENSION "+fileUrl);
			return true;
		}
	}
	
	return false;
};

Europeana.prototype.GetSound = function(currentDomain, visited, fileUrl, domain, allowRecursion, onComplete) {
	myConsole.Log("GetSound "+fileUrl);
	
	if (!utils.ObjectValid(fileUrl) || fileUrl.indexOf("#") > -1) {
		myConsole.Log("GetSound url error: "+fileUrl);
		if (utils.ObjectValid(onComplete)) {
			onComplete(null);
		}
		return;
	}

	var theUrl = fileUrl;
	var completeFunc = onComplete;
	var canRecurse = allowRecursion;
	var theDomain = domain;
	var domainCurrent = currentDomain;
	var visitedList = visited;

	if (europeana.URLHasValidSoundExtension(theUrl)) {
		myConsole.Log("Sound URL has a valid extension! "+theUrl);

		europeana.CheckURLExists(theUrl, function(exists, error, response, body) {
			if (exists) {
				var sound = new Object;
				sound.url = theUrl;
	
				if (utils.ObjectValid(completeFunc)) {
					completeFunc(sound);
				}
			} else {
				myConsole.Log("Sound page does not exist! "+theUrl);

				if (utils.ObjectValid(completeFunc)) {
					completeFunc(null);
				}
			}
		});
	} else if (canRecurse && europeana.CanRecursivelyCheckURL(domainCurrent, visitedList, theUrl)) {
		myConsole.Log("Sound page does exist, and I'm allowed to check it... "+theUrl);

		europeana.RequestAndGetSoundInPage(domainCurrent, visitedList, theUrl, theDomain, false, function(sound) {
			if (!utils.ObjectValid(sound)) {
				if (utils.ObjectValid(completeFunc)) {
					completeFunc(null);
				}
			} else {
				var theSound = sound;
				europeana.CheckURLExists(sound.url, function(exists, error, response, body) {
					if (exists) {
						if (utils.ObjectValid(completeFunc)) {
							completeFunc(theSound);
						}
					} else {
						myConsole.Log("Sound page does not exist! "+theUrl);

						if (utils.ObjectValid(completeFunc)) {
							completeFunc(null);
						}
					}
				});
			}
		});
	} else {
		myConsole.Log("Visited URL before: "+theUrl);

		if (utils.ObjectValid(completeFunc)) {
			completeFunc(null);
		}
	}
};

Europeana.prototype.RequestAndGetSoundInPage = function(currentDomain, visited, pageUrl, domain, allowRecursion, onComplete) {
	var completeFunc = onComplete;
	var theDomain = domain;
	
	var domainCurrent = currentDomain;
	var visitedList = visited;

	europeana.VisitedURL(pageUrl);

	var pageRequest = request({
		uri: pageUrl,
	}, function(error, response, body) {
		myConsole.Log("Getting sounds in page "+pageUrl);
		europeana.GetSoundInPage(domainCurrent, visitedList, cheerio.load(body), theDomain, allowRecursion, function(sound) {
			myConsole.Log("out of GetSoundInPage in RequestAndGetSoundInPage, sound: "+sound);

			if (utils.ObjectValid(completeFunc)) {
				myConsole.Log("RequestAndGetSoundInPage completefunc "+sound);
				completeFunc(sound);
			}
		});
	});
	
	return pageRequest;
};

Europeana.prototype.GetSoundInPage = function(currentDomain, visited, body, domain, allowRecursion, onComplete) {
	var $ = body;
	var theDomain = domain;
	var completeFunc = onComplete;
	var canRecurse = allowRecursion;

	var sounds = new Array();
	var soundElements = $("sound");
	for (var s = 0; s < soundElements.length; s++) {
		var element = $(soundElements[s]);
		element.type = "sound";

		myConsole.Log("Got sound");
		sounds.push(element);
	}
	soundElements = $("a");
	for (var s = 0; s < soundElements.length; s++) {
		var element = $(soundElements[s]);
		element.type = "a";
		
		myConsole.Log("Got a "+element.attr("href"));
		sounds.push(element);
	}
	
	var theDomain = currentDomain;
	var visitedList = visited;

	if (sounds.length <= 0) {
		if (utils.ObjectValid(completeFunc)) {
			completeFunc(null);
		}
	} else {
		var soundData = new Array();
	
		var soundIndex = 0;
		for (var s = 0; s < sounds.length; s++) {
			var element = sounds[s];
	
			var src = null;		
			if (element.type === "sound") {
				src = element.attr("src");
				myConsole.Log("Checking sound src: "+src);
			} else if (element.type === "a") {
				src = element.attr("href");
//				myConsole.Log("Checking a href: "+src);
				if (europeana.URLHasValidSoundExtension(src)) {
					myConsole.Log("Apparently URL is correct... A: "+src);
				} else if (canRecurse && utils.ObjectValid(src) && (src.indexOf("PlaySound") > -1 || src.indexOf("SoundID") > -1)) {
					myConsole.Log("Apparently URL is correct... B: "+src);
				} else {
					myConsole.Log("Apparently URL is not correct... C: "+src);
					src = null;
					soundIndex++;
					continue;
				}
			}

			if (!utils.ObjectValid(src) || src.length <= 0) {
				soundIndex++;
				continue;
			}
			
			myConsole.Log("Sound url: "+src);
			
			var fullAddress = europeana.GetAbsoluteURL(theDomain, src);

			myConsole.Log("Sound address: "+fullAddress+" (domain: "+theDomain+")");
		
			europeana.GetSound(theDomain, visitedList, fullAddress, theDomain, canRecurse, function(sound) {
				europeana.VisitedURL(fullAddress);
				
				var showUrl = null;
				if (utils.ObjectValid(sound)) {
					showUrl = sound.url;
					soundData.push(sound);
				}
			
				soundIndex++;

				if (utils.ObjectValid(showUrl)) {
					myConsole.Log("GotSound "+showUrl+" index: "+soundIndex+" total: "+sounds.length);
				} else {
					myConsole.Log("Didn't GetSound "+fullAddress+" index: "+soundIndex+" total: "+sounds.length);
				}
			
				if (soundIndex >= sounds.length) {
					if (utils.ObjectValid(completeFunc)) {
						myConsole.Log("Getting best sound from array with length "+soundData.length);
						var theSound = europeana.GetBestSoundFromArray(soundData);
						myConsole.Log("Best sound: "+theSound+", returning: "+completeFunc);
						completeFunc(theSound);
					}
				}
			});
		}
	}
};

Europeana.prototype.GetBestSoundFromArray = function(soundArray) {
	myConsole.Log("GetBestSoundFromArray "+soundArray.length);

	var best = null;
	if (soundArray.length > 0) {
		for (var e = 0; e < europeana.EXTENSIONS.AUDIO.length; e++) {
			var extension = "."+europeana.EXTENSIONS.AUDIO[e].toLowerCase();
			var foundIt = false;
			for (var s = 0; s < soundArray.length; s++) {
				best = soundArray[0];
				if (!utils.ObjectsValid([best, best.url])) {
					best = null;
					continue;
				}
				var theUrl = best.url.toLowerCase();
//				myConsole.Log("Testing url: "+theUrl+" against extension: "+extension);
				if (theUrl.indexOf(extension) > -1) {
//					myConsole.Log("Match!");
					foundIt = true;
					break;
				} else {
//					myConsole.Log("No match!");
					best = null;
				}
			}
			
			if (foundIt) {
				break;
			}
		}
	}
	
	return best;
};

Europeana.prototype.RespondWithSoundData = function(response, sound) {
	if (!utils.ObjectsValid([response, sound])) {
		return false;
	}
		
	europeana.SendResponse(response, {status: europeana.ERRORS.NONE, location: sound.url});
	return true;
};

Europeana.prototype.FinishRecordSound = function(response, sound) {
	if (!utils.ObjectValid(sound)) {
		europeana.SendResponse(response, {status: europeana.ERRORS.NONE_FOUND, contentType: europeana.MIME.TEXT, data: "Couldn't get a sound!"});
		return;
	}

	if (!europeana.RespondWithSoundData(response, sound)) {
		europeana.SendResponse(response, {status: europeana.ERRORS.RETRIEVING, contentType: europeana.MIME.TEXT, data: "Tried to get the sound in the page but something went wrong!"});
	}
};

Europeana.prototype.GetRecordSound = function(currentDomain, visited, response, recordId) {
	var theRes = response;
	var theId = recordId;
	
	var theDomain = currentDomain;
	var visitedList = visited;

	var queryString = europeana.APIURL+"/record"+recordId+".json?wskey="+europeana.KEY+"&profile="+europeana.PROFILES.RECORD;
	europeana.Query(queryString, function(response, data) {
		var resultObject = null;
		try {
			resultObject = JSON.parse(data);
		} catch (error) {
			europeana.FinishRecordSound(theRes, null);
			return;		
		}

		var item = europeana.GetObjectData("minimal", theId, resultObject);
		if (utils.ObjectValid(item.unretrievable)) {
			europeana.CantGetAsset(theRes, item.unretrievable);
			return;
		}

		if (item.type.toLowerCase() !== "sound") {
			europeana.FinishRecordSound(theRes, null);
			return;		
		}
		
		europeana.GetSound(theDomain, visitedList, item.url, null, false, function(sound) {
			europeana.VisitedURL(item.url);

			if (utils.ObjectValid(sound)) {
				europeana.FinishRecordSound(theRes, sound);
			} else {
				europeana.GetSound(theDomain, visitedList, item.object, null, false, function(sound) {
					europeana.VisitedURL(item.object);

					if (utils.ObjectValid(sound)) {
						europeana.FinishRecordSound(theRes, sound);
					} else {
						if (!utils.ObjectValid(item.page)) {
							europeana.FinishRecordSound(theRes, null);
						} else {
							if (!utils.ObjectValid(item.page)) {
								europeana.FinishRecordSound(theRes, siteImages);
							} else {
								myConsole.Log("Scraping page "+item.page);

								europeana.VisitedURL(item.page);
								
								var soundRequest = request({
									uri: item.page,
								}, function(error, response, body) {
									body = cheerio.load(body);
									var $ = body;
									
									
									var domain = europeana.GetBaseURLOfPage(item.page, body, response);
									theDomain = europeana.SetCurrentDomain(domain);
						
									myConsole.Log("Domain: "+domain);
				
									myConsole.Log("Getting sounds in page "+item.page);
									europeana.GetSoundInPage(theDomain, visitedList, body, domain, true, function(sound) {
										myConsole.Log("out of GetSoundInPage in GetRecordSound, sound: "+sound);
									
										if (utils.ObjectValid(sound)) {
											myConsole.Log("GetRecordSound FinishRecordSound "+sound);
											europeana.FinishRecordSound(theRes, sound);
										} else {
											var frames = $("frame");
											
											if (frames.length <= 0) {
												europeana.FinishRecordSound(theRes, null);
											} else {
												var framesSounds = new Array();
				
												var frameIndex = 0;
				
												frames.each(function() {
													var frame = $(this);
					
													var src = frame.attr("src");
					
													var address = null;
													if (src.indexOf("http") === 0) {
														address = src;
													} else if (utils.ObjectValid(domain)) {
														address = domain+"/"+src;
													}
					
													var frameRequest = europeana.RequestAndGetSoundInPage(theDomain, visitedList, address, domain, true, function(sound) {
														if (utils.ObjectValid(sound)) {
															framesSounds.push(sound);
														}
											
														frameIndex++;
										
														if (frameIndex >= frames.length) {
															var theSound = europeana.GetBestSoundFromArray(framesSounds);
															europeana.FinishRecordSound(theRes, theSound);
														}
													});
												});
											}
										}
									});
								});
							}
						}
					}
				});
			}
		});
	});
};
