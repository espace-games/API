﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestAPICalls : MonoBehaviour {

	public Europeana europeana = null;
	public GameObject[] paintings;
	public GUIText[] labels;

	public GameObject[] secondPaintings;
	public GUIText[] secondLabels;

	public GameObject singlePainting;
	public GUIText singleLabel;

	private EuropeanaRequest myRequest = null;

	void Start () {
		string searchTerms = "classical painting";
		string secondTerms = "beaver";
		string singleId = "/90402/SK_A_190"; // Not future proof!
		string offset = "0";
		string width = "1024";
		string height = "768";
		string minWidth = "512";
		string minHeight = "256";
		string keepAspect = Europeana.VALUE_TRUE;
		string pow2 = Europeana.VALUE_TRUE;
		string cropPercentage = "10";
		string textureSize = "1024";
		string verboseLogging = Europeana.VALUE_TRUE;

		Debug.Log ("Retrieving Europeana images with terms: " + searchTerms);

		myRequest = europeana.GetImages(new Dictionary<string, string>() {
			{ Europeana.OPTION_SEARCH, searchTerms },
			{ Europeana.OPTION_COUNT, paintings.Length.ToString () },
			{ Europeana.OPTION_OFFSET, offset },
			{ Europeana.OPTION_WIDTH, width },
			{ Europeana.OPTION_HEIGHT, height },
			{ Europeana.OPTION_MIN_WIDTH, minWidth },
			{ Europeana.OPTION_MIN_HEIGHT, minHeight },
			{ Europeana.OPTION_KEEP_ASPECT, keepAspect },
			{ Europeana.OPTION_POWER_OF_2, pow2 },
			{ Europeana.OPTION_CROP_PERCENTAGE, cropPercentage },
			{ Europeana.OPTION_TEXTURE_SIZE, textureSize },
			{ Europeana.OPTION_VERBOSE_LOGGING, verboseLogging }
		}, image => {
			Debug.Log("Retrieved image: "+image.Id+" index: "+image.Index);

			if (image.Index >= paintings.Length) {
				return;
			}

			GameObject painting = paintings[image.Index];
			GUIText label = labels[image.Index];
			
			string newLabel = "No title or attribution data";
			if (image.Title != null) {
				newLabel = image.Title;
				if (image.Attribution != null) {
					newLabel += "\n";
				}
			}
			if (image.Attribution != null) {
				newLabel += image.Attribution;
			}
			
			label.text = newLabel;

			try {
				painting.renderer.material.mainTexture = image.TextureData;
			} catch {
				Debug.Log("Error assigning texture for image "+image.Id);
			}

		}, images => {
			Debug.Log("Finished retrieving images: "+images.Count);

			if (images.Count < labels.Length) {
				for (var l = labels.Length-1; l >= images.Count; l--) {
					GUIText label = labels[l];
					label.text = "Couldn't get image!";
				}
			}
		}, error => {
			Debug.Log("Got an error message! It follows:");
			string message = europeana.PrintError(error);

			for (var l = 0; l < labels.Length; l++) {
				GUIText label = labels[l];
				label.text = "Error: "+message;
			}
		});

		europeana.GetImages(new Dictionary<string, string>() {
			{ Europeana.OPTION_SEARCH, secondTerms },
			{ Europeana.OPTION_COUNT, secondPaintings.Length.ToString () },
			{ Europeana.OPTION_OFFSET, offset },
			{ Europeana.OPTION_WIDTH, width },
			{ Europeana.OPTION_HEIGHT, height },
			{ Europeana.OPTION_MIN_WIDTH, minWidth },
			{ Europeana.OPTION_MIN_HEIGHT, minHeight },
			{ Europeana.OPTION_KEEP_ASPECT, keepAspect },
			{ Europeana.OPTION_POWER_OF_2, pow2 },
			{ Europeana.OPTION_CROP_PERCENTAGE, cropPercentage },
			{ Europeana.OPTION_TEXTURE_SIZE, textureSize },
			{ Europeana.OPTION_VERBOSE_LOGGING, verboseLogging }
		}, image => {
			Debug.Log("SECOND REQUEST Retrieved image: "+image.Id+" index: "+image.Index);

			if (image.Index >= secondPaintings.Length) {
				return;
			}
			
			GameObject painting = secondPaintings[image.Index];
			GUIText label = secondLabels[image.Index];
			
			string newLabel = "No title or attribution data";
			if (image.Title != null) {
				newLabel = image.Title;
				if (image.Attribution != null) {
					newLabel += "\n";
				}
			}
			if (image.Attribution != null) {
				newLabel += image.Attribution;
			}
			
			label.text = newLabel;
			
			try {
				painting.renderer.material.mainTexture = image.TextureData;
			} catch {
				Debug.Log("SECOND REQUEST Error assigning texture for image "+image.Id);
			}
		}, images => {
			Debug.Log("SECOND REQUEST Finished retrieving images: "+images.Count);
		}, error => {
			Debug.Log("SECOND REQUEST Got an error message! It follows:");
			string message = europeana.PrintError(error);
			
			for (var l = 0; l < secondLabels.Length; l++) {
				GUIText label = secondLabels[l];
				label.text = "Error: "+message;
			}
		});

		StartCoroutine (TestCancel ());

		europeana.GetImage (new Dictionary<string, string>() {
			{ Europeana.OPTION_ID, singleId },
			{ Europeana.OPTION_WIDTH, width },
			{ Europeana.OPTION_HEIGHT, height },
			{ Europeana.OPTION_KEEP_ASPECT, keepAspect },
			{ Europeana.OPTION_POWER_OF_2, pow2 },
			{ Europeana.OPTION_CROP_PERCENTAGE, cropPercentage },
			{ Europeana.OPTION_TEXTURE_SIZE, textureSize },
			{ Europeana.OPTION_VERBOSE_LOGGING, verboseLogging }
		}, image => {
			Debug.Log("SINGLE REQUEST Retrieved image: "+image.Id);
			
			GameObject painting = singlePainting;
			GUIText label = singleLabel;
			
			string newLabel = "No title or attribution data";
			if (image.Title != null) {
				newLabel = image.Title;
				if (image.Attribution != null) {
					newLabel += "\n";
				}
			}
			if (image.Attribution != null) {
				newLabel += image.Attribution;
			}
			
			label.text = newLabel;
			
			try {
				painting.renderer.material.mainTexture = image.TextureData;
			} catch {
				Debug.Log("SINGLE REQUEST Error assigning texture for image "+image.Id);
			}
		}, error => {
			Debug.Log("SINGLE REQUEST Got an error message! It follows:");
			string message = europeana.PrintError(error);
			
			GUIText label = singleLabel;
			label.text = "Error: "+message;
		});

	}

	private IEnumerator TestCancel () {
		yield return new WaitForSeconds(1);
		myRequest.Cancel ();
		myRequest = null;
	}
}
